package edu.ntnu.stud.Model.ChaosGame;

import edu.ntnu.stud.Model.ChaosCanvas;
import edu.ntnu.stud.Model.ChaosGameDescription.ChaosGameDescription;
import edu.ntnu.stud.Model.Transformation.Transform;
import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * JUnit tests for the ChaosGameManager class, covering both positive and negative scenarios.
 */
class ChaosGameManagerTest {
  private ChaosGameManager manager;
  private ChaosGameDescription description;
  private ChaosCanvas canvas;

  @BeforeEach
  void setUp() {
    List<Transform> transforms = new ArrayList<>();
    description = new ChaosGameDescription(transforms, new Vector2D(new double[]{0.0, 0.0}), new Vector2D(new double[]{10.0, 10.0}));

    manager = new ChaosGameManager(description, 100, 100);
    canvas = manager.getCanvas();
  }

  /** Positive test cases for ChaosGameManager. */
  @Nested
  @DisplayName("Positive tests for ChaosGameManager class.")
  class PositiveTests {

    @Test
    @DisplayName("Test constructor with valid inputs.")
    void constructor_ValidInputs_CreatesInstance() {
      assertNotNull(manager);
    }

    @Test
    @DisplayName("Test getCanvas method.")
    void getCanvas_ReturnsCanvas() {
      assertNotNull(manager.getCanvas());
    }

    @Test
    @DisplayName("Test getCountOfTransforms with empty transforms.")
    void getCountOfTransforms_EmptyTransforms_ReturnsZero() {
      assertEquals(0, manager.getCountOfTransforms());
    }

    @Test
    @DisplayName("Test putPixel with valid coordinates.")
    void putPixel_ValidCoords_IncrementsPixelValue() {
      Vector point = new Vector2D(new double[]{5.0, 5.0});
      manager.putPixel(point);
      assertEquals(1, canvas.getPixel(point));
    }

    @Test
    @DisplayName("Test setCanvasSize with valid size.")
    void setCanvasSize_ValidSize_UpdatesCanvasSize() {
      manager.setCanvasSize(200);
      assertEquals(200, manager.getCanvas().getCanvasArray().length);
    }
  }

  /** Negative test cases for ChaosGameManager. */
  @Nested
  @DisplayName("Negative tests for ChaosGameManager class.")
  class NegativeTests {

    @Test
    @DisplayName("Test constructor with null description.")
    void constructor_NullDescription_ThrowsIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> new ChaosGameManager(null, 100, 100));
    }

    @Test
    @DisplayName("Test putPixel with invalid coordinates type.")
    void putPixel_InvalidCoordsType_ThrowsIllegalArgumentException() {
      Vector invalidCoords = new Vector() {
        @Override
        public double[] getVector() {
          return new double[0];
        }

        @Override
        public Vector add(Vector vector) {
          return null;
        }

        @Override
        public Vector subtract(Vector vector) {
          return null;
        }
      };
      assertThrows(IllegalArgumentException.class, () -> manager.putPixel(invalidCoords));
    }

    @Test
    @DisplayName("Test setChaosGameDescription with null description.")
    void setChaosGameDescription_NullDescription_ThrowsIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> manager.setChaosGameDescription(null));
    }

    @Test
    @DisplayName("Test setCanvasSize with invalid size.")
    void setCanvasSize_InvalidSize_ThrowsIllegalArgumentException() {
      assertThrows(NegativeArraySizeException.class, () -> manager.setCanvasSize(-100));
    }
  }
}