package edu.ntnu.stud.View;

import edu.ntnu.stud.Controller.ExceptionHandler;
import edu.ntnu.stud.Controller.FractalHandler;
import edu.ntnu.stud.Controller.Observer;
import edu.ntnu.stud.Controller.SceneNavigation;
import edu.ntnu.stud.Model.ChaosCanvas;
import edu.ntnu.stud.View.GUIComponents.ActionFeedback;
import edu.ntnu.stud.View.GUIComponents.NavBar;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * Main page for the application.
 */
public class MainPage implements Observer {
  private final SceneNavigation sceneNavigation;
  private final FractalHandler fractalHandler;
  private final NavBar navBar;
  private final Scene mainPageScene;
  private final BorderPane mainPageRoot;
  private final StackPane centerPane;
  private final VBox fractalView;
  private int windowSize;
  private final int fractalMargin = 100;

  /**
   * Constructor for MainPage.
   *
   * @param sceneNavigation object of type SceneNavigation
   * @param windowHeight    the initial height of the window
   * @param windowWidth     the initial width of the window
   */
  public MainPage(SceneNavigation sceneNavigation, int windowHeight, int windowWidth) {
    this.windowSize = Math.min(windowHeight, windowWidth);
    this.sceneNavigation = sceneNavigation;
    this.navBar = NavBar.getInstance();
    this.fractalHandler = new FractalHandler(windowSize - 2 * fractalMargin);
    this.fractalHandler.addObserver(this);

    this.mainPageRoot = new BorderPane();
    this.centerPane = new StackPane();
    this.fractalView = new VBox();

    this.mainPageRoot.setCenter(centerPane);
    this.mainPageRoot.setMinHeight(this.windowSize);
    this.mainPageRoot.setMinWidth(this.windowSize);
    this.mainPageRoot.setStyle("-fx-background-color: black;");
    this.mainPageScene = new Scene(mainPageRoot);
    this.mainPageScene.getStylesheets().add("/Stylesheets/generalStyles.css");

    addToRoot();
  }

  /**
   * Resize the window.
   *
   * @param height the new height of the window
   * @param width  the new width of the window
   */
  public void windowResize(int height, int width) {
    this.windowSize = Math.min(height, width);
    this.fractalHandler.fractalResize(this.windowSize - 2 * this.fractalMargin);
  }

  /**
   * Add the elements to the root.
   */
  private void addToRoot() {
    mainPageRoot.getChildren().clear();
    centerPane.getChildren().clear();

    mainPageRoot.setTop(navBar.getNavBar());
    centerPane.getChildren().add(fractalView);
    mainPageRoot.setCenter(centerPane);
  }

  /**
   * Generate a 2D fractal based on a ChaosGame.
   *
   * @param canvas the ChaosCanvas object containing the fractal data
   */
  private void addFractal2dToFractalView(ChaosCanvas canvas) {
    this.fractalView.getChildren().clear();
    this.fractalView.setPadding(
        new Insets(
            this.fractalMargin / 1.5, this.fractalMargin, this.fractalMargin, this.fractalMargin));

    this.fractalView
        .getChildren()
        .add(generateFractal2dCanvas(canvas.getCanvasArray(), canvas.getMaxPixelValue()));
  }

  /**
   * Fill the graphics context with the image.
   *
   * @param graphicsContext object of type GraphicsContext
   * @param image           the image to fill the graphics context with
   * @param maxPixelValue   the maximum pixel value in the image
   */
  private void fillGraphicsContext(
      GraphicsContext graphicsContext, int[][] image, int maxPixelValue) {
    try {
      for (int y = 0; y < image.length; y++) {
        for (int x = 0; x < image[y].length; x++) {
          double percentageOfMaxHits = (double) image[y][x] / maxPixelValue;
          if (percentageOfMaxHits > 1) {
            percentageOfMaxHits = 1;
          }
          graphicsContext.setFill(
              image[y][x] == 0 ? Color.BLACK : interpolateColor(percentageOfMaxHits));
          graphicsContext.fillRect(x, y, 1, 1);
        }
      }
    } catch (Exception e) {
      ExceptionHandler.getInstance()
          .logException(e, "An error occurred while filling the graphics context.");
      Platform.runLater(() -> ActionFeedback.showErrorDialogBox(
          "An error occurred while filling the graphics context: " + e.getMessage()));
    }
  }

  /**
   * Interpolate a color based on the percentage of max hits.
   *
   * @param percentageOfMaxHits the percentage of max hits
   * @return a Color object
   */
  private Color interpolateColor(double percentageOfMaxHits) {
    Color[] colors = {Color.BLUE, Color.YELLOW, Color.RED, Color.WHITE};

    int numColors = colors.length;
    double segmentLength = 1.0 / (numColors - 1);
    int segmentIndex = (int) (percentageOfMaxHits / segmentLength);
    if (segmentIndex >= numColors - 1) {
      segmentIndex = numColors - 2;
    }

    double segmentStart = segmentIndex * segmentLength;
    double segmentEnd = (segmentIndex + 1) * segmentLength;
    double segmentPosition = (percentageOfMaxHits - segmentStart) / (segmentEnd - segmentStart);

    Color startColor = colors[segmentIndex];
    Color endColor = colors[segmentIndex + 1];

    double r = startColor.getRed() + segmentPosition * (endColor.getRed() - startColor.getRed());
    double g =
        startColor.getGreen() + segmentPosition * (endColor.getGreen() - startColor.getGreen());
    double b = startColor.getBlue() + segmentPosition * (endColor.getBlue() - startColor.getBlue());

    return new Color(r, g, b, 1.0);
  }

  /**
   * Generate a fractal 2D canvas.
   *
   * @param image         the image to generate the canvas from
   * @param maxPixelValue the maximum pixel value in the image
   * @return a Canvas object
   */
  private Canvas generateFractal2dCanvas(int[][] image, int maxPixelValue) {
    Canvas canvas = new Canvas(image[0].length, image.length);
    GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
    fillGraphicsContext(graphicsContext, image, maxPixelValue);
    return canvas;
  }

  /**
   * Get the main page scene.
   *
   * @return the main page scene
   */
  public Scene getMainPageScene() {
    return mainPageScene;
  }

  /**
   * Add a progress indicator to the center of the main page.
   */
  private void addProgressIndicator() {
    HBox progressIndicatorHbox = new HBox();
    ProgressIndicator loadingFractalProgress = new ProgressIndicator();
    progressIndicatorHbox.getChildren().addAll(loadingFractalProgress);
    centerPane.getChildren().add(progressIndicatorHbox);
  }

  /**
   * Remove the progress indicator from the center of the main page.
   */
  private void removeProgressIndicator() {
    centerPane.getChildren().clear();
    centerPane.getChildren().add(fractalView);
  }

  /**
   * Update based on a change in other objects that are observed.
   *
   * @param changedValueName the name of the changed value
   * @param newValue         the new value
   */
  @Override
  public void update(String changedValueName, Object newValue) {
    try {
      switch (changedValueName) {
        case "ChaosGameComplete" -> {
          if (newValue instanceof ChaosCanvas) {
            Platform.runLater(() -> {
              removeProgressIndicator();
              addFractal2dToFractalView((ChaosCanvas) newValue);
              this.addToRoot();
            });
          } else {
            throw new IllegalArgumentException(
                "Expected ChaosCanvas but received: " + newValue.getClass());
          }
        }
        case "runningChaosGame" -> Platform.runLater(this::addProgressIndicator);
        default -> {
        }
      }
    } catch (Exception e) {
      ExceptionHandler.getInstance().logException(e, "An error occurred while updating MainPage.");
      Platform.runLater(
          () -> ActionFeedback.showErrorDialogBox("An error occurred: " + e.getMessage()));
    }
  }
}
