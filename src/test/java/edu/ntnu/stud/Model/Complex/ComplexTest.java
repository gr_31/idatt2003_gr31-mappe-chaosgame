package edu.ntnu.stud.Model.Complex;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.stud.Model.Vector.Complex;
import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ComplexTest {

  private Complex complex1;
  private Complex complex2;

  /**
   * Initializes two complex numbers available for all test methods in this class
   */
  @BeforeEach
  public void initTwoComplex() {
    complex1 = new Complex(1.0, 2.0);
    complex2 = new Complex(-1.0, 3.0);
  }

  /**
   * Positive test cases for Complex.
   */
  @Nested
  @DisplayName("Positive tests for Complex class.")
  class PositiveTests {

    @Test
    @DisplayName("Test displayComplexNumber with valid complex number.")
    void displayComplexNumber_validComplex_displayCorrectly() {
      assertEquals("1.0 + 2.0i", complex1.displayComplexNumber());
    }

    @Test
    @DisplayName("Test add method with valid complex numbers.")
    void add_validComplexNumbers_returnCorrectSum() {
      Complex expectedResult = new Complex(0.0, 5.0);
      assertEquals(expectedResult, complex1.add(complex2));
    }

    @Test
    @DisplayName("Test subtract method with valid complex numbers.")
    void subtract_validComplexNumbers_returnCorrectDifference() {
      Complex expectedResult = new Complex(2.0, -1.0);
      assertEquals(expectedResult, complex1.subtract(complex2));
    }

    @Test
    @DisplayName("Test sqrt method with valid complex number.")
    void sqrt_validComplexNumber_returnCorrectSqrt() {
      Complex expectedResult = new Complex(1.272019649514069, 0.7861513777574233);
      assertEquals(expectedResult, complex1.sqrt());
    }

    @Test
    @DisplayName("Test equals method with same instance.")
    void equals_sameInstance_returnTrue() {
      assertEquals(complex1, complex1);
    }

    @Test
    @DisplayName("Test equals method with different instances, same values.")
    void equals_differentInstancesSameValues_returnTrue() {
      Complex complex2 = new Complex(1.0, 2.0);
      assertEquals(complex1, complex2);
    }
  }

  /**
   * Negative test cases for Complex.
   */
  @Nested
  @DisplayName("Negative tests for Complex class.")
  class NegativeTests {

    @Test
    @DisplayName("Test add method with different instances, different values.")
    void add_differentInstancesDifferentValues_returnFalse() {
      Complex differentComplex = new Complex(2.0, 3.0);
      assertNotEquals(complex1.add(complex2), differentComplex.add(complex2));
    }

    @Test
    @DisplayName("Test subtract method with different instances, different values.")
    void subtract_differentInstancesDifferentValues_returnFalse() {
      Complex differentComplex = new Complex(2.0, 3.0);
      assertNotEquals(complex1.subtract(complex2), differentComplex.subtract(complex2));
    }

    @Test
    @DisplayName("Test equals method with different instances, different values.")
    void equals_differentInstancesDifferentValues_returnFalse() {
      Complex differentComplex = new Complex(1.0, 3.0);
      assertNotEquals(complex1, differentComplex);
    }

    @Test
    @DisplayName("Test add method with non-Complex vector.")
    void add_nonComplexVector_throwIllegalArgumentException() {
      Vector nonComplexVector = new Vector2D(new double[] {1.0, 2.0});
      assertThrows(IllegalArgumentException.class, () -> complex1.add(nonComplexVector));
    }

    @Test
    @DisplayName("Test subtract method with non-Complex vector.")
    void subtract_nonComplexVector_throwIllegalArgumentException() {
      Vector nonComplexVector = new Vector2D(new double[] {1.0, 2.0});
      assertThrows(IllegalArgumentException.class, () -> complex1.subtract(nonComplexVector));
    }
  }
}
