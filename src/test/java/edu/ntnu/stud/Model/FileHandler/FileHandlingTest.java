package edu.ntnu.stud.Model.FileHandler;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.stud.Model.ChaosGameDescription.ChaosGameDescription;
import edu.ntnu.stud.Model.FileHandling;
import edu.ntnu.stud.Model.Matrix.Matrix;
import edu.ntnu.stud.Model.Matrix.Matrix2D;
import edu.ntnu.stud.Model.Transformation.AffineTransform2D;
import edu.ntnu.stud.Model.Transformation.JuliaTransform;
import edu.ntnu.stud.Model.Transformation.Transform;
import edu.ntnu.stud.Model.Vector.Complex;
import edu.ntnu.stud.Model.Vector.Vector2D;
import edu.ntnu.stud.Model.Vector.Vector;
import java.util.ArrayList;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

class FileHandlingTest {
  @Nested
  @DisplayName("Tests for reading from files")
  class readFromFile {
    @Nested
    @DisplayName("Positive tests for reading from files")
    class PositiveTests {
      /** Test if FileHandling reads and converts AffineTestFile correctly. */
      @Test
      @DisplayName("Test readAffineTestFile method with correct file.")
      void readAffineTestFile_CorrectFile_ValidResult() throws Exception {
        ChaosGameDescription descriptionFromFile =
            FileHandling.readFromFile(
                "src/test/java/edu/ntnu/stud/Model/FileHandler/TestFractals/AffineTestFile");

        Vector minCoords = new Vector2D(new double[] {0, 0});
        Vector maxCoords = new Vector2D(new double[] {1, 1});

        Matrix affineMatrix = new Matrix2D(new double[][] {{0.5, 0}, {0, 0.5}});
        Vector affineVector1 = new Vector2D(new double[] {0, 0});
        Vector affineVector2 = new Vector2D(new double[] {0.25, 0.5});
        Vector affineVector3 = new Vector2D(new double[] {0.5, 0});

        List<Transform> expectedTransforms = new ArrayList<>();
        expectedTransforms.add(new AffineTransform2D(affineMatrix, affineVector1));
        expectedTransforms.add(new AffineTransform2D(affineMatrix, affineVector2));
        expectedTransforms.add(new AffineTransform2D(affineMatrix, affineVector3));

        ChaosGameDescription expectedDescription =
            new ChaosGameDescription(expectedTransforms, minCoords, maxCoords);

        assertEquals(expectedDescription, descriptionFromFile);
      }

      /** Test if FileHandling reads and converts JuliaTestFile correctly. */
      @Test
      @DisplayName("Test readJuliaTestFile method with correct file.")
      void readJuliaTestFile_CorrectFile_ValidResult() throws Exception {
        ChaosGameDescription descriptionFromFile =
            FileHandling.readFromFile(
                "src/test/java/edu/ntnu/stud/Model/FileHandler/TestFractals/JuliaTestFile");

        Vector minCoords = new Vector2D(new double[] {-1.6, -1});
        Vector maxCoords = new Vector2D(new double[] {1.6, 1});

        List<Transform> expectedTransforms = new ArrayList<>();
        expectedTransforms.add(new JuliaTransform(new Complex(-0.74543, 0.11301), 1));
        expectedTransforms.add(new JuliaTransform(new Complex(-0.74543, 0.11301), -1));

        ChaosGameDescription expectedDescription =
            new ChaosGameDescription(expectedTransforms, minCoords, maxCoords);

        assertEquals(expectedDescription, descriptionFromFile);
      }
    }

    @Nested
    @DisplayName("Negative tests for reading from files")
    class NegativeTests {
      /**
       * Test if FileHandling throws an exception when reading from a file with incorrect
       * transformation type.
       */
      @Test
      @DisplayName("Test readFromFile method with file with incorrect format.")
      void readFromFile_IncorrectTransformName_ExceptionThrown() {
        assertThrows(
            IllegalArgumentException.class,
            () ->
                FileHandling.readFromFile(
                    "src/test/java/edu/ntnu/stud/Model/FileHandler/TestFractals/FractalIncorrectTransformType"));
      }

      /**
       * Test if FileHandling throws an exception when reading from a file with affine
       * transformation with too few lines.
       */
      @Test
      @DisplayName("Test readFromFile method with affine transformation file with too few lines.")
      void readAffineFromFile_TooFewLines_ExceptionThrown() {
        assertThrows(
            IllegalArgumentException.class,
            () ->
                FileHandling.readFromFile(
                    "src/test/java/edu/ntnu/stud/Model/FileHandler/TestFractals/AffineFractalTooFewLines"));
      }

      /**
       * Test if FileHandling throws an exception when reading from a file with julia transformation
       * with too few lines.
       */
      @Test
      @DisplayName("Test readFromFile method with julia transformation file with too few lines.")
      void readJuliaFromFile_TooFewLines_ExceptionThrown() {
        assertThrows(
            IllegalArgumentException.class,
            () ->
                FileHandling.readFromFile(
                    "src/test/java/edu/ntnu/stud/Model/FileHandler/TestFractals/JuliaFractalTooManyLines"));
      }

      /** Test if FileHandling throws an exception when reading from a file with text in doubles. */
      @Test
      @DisplayName("Test readFromFile method with file with text in doubles.")
      void readFromFile_TextInDoubles_ExceptionThrown() {
        assertThrows(
            IllegalArgumentException.class,
            () ->
                FileHandling.readFromFile(
                    "src/test/java/edu/ntnu/stud/Model/FileHandler/TestFractals/AffineTestFileIncorrectStringInDouble"));
      }

      /**
       * Test if FileHandling throws an exception when reading from a file with incorrect count of
       * doubles.
       */
      @Test
      @DisplayName("Test readFromFile method with file with incorrect count of doubles.")
      void readFromFile_IncorrectCountOfDoubles_ExceptionThrown() {
        assertThrows(
            IllegalArgumentException.class,
            () ->
                FileHandling.readFromFile(
                    "src/test/java/edu/ntnu/stud/Model/FileHandler/TestFractals/AffineTestFileIncorrectCountOfDoubles"));
      }
    }
  }

  @Nested
  @DisplayName("Tests for writing to files")
  class writeToFile {
    /** Test if FileHandling writes AffineTestFile correctly. */
    @Test
    @DisplayName("Test writeToFile method with AffineTestFile.")
    void writeToFile_AffineTestFile_ValidResult() throws Exception {
      Matrix affineMatrix = new Matrix2D(new double[][] {{0.5, 0}, {0, 0.5}});
      Vector affineVector1 = new Vector2D(new double[] {0, 0});
      Vector affineVector2 = new Vector2D(new double[] {0.25, 0.5});
      Vector affineVector3 = new Vector2D(new double[] {0.5, 0});

      List<Transform> transforms = new ArrayList<>();
      transforms.add(new AffineTransform2D(affineMatrix, affineVector1));
      transforms.add(new AffineTransform2D(affineMatrix, affineVector2));
      transforms.add(new AffineTransform2D(affineMatrix, affineVector3));

      ChaosGameDescription description =
          new ChaosGameDescription(transforms, affineVector1, affineVector3);

      FileHandling.writeToFile(
          description,
          "src/test/java/edu/ntnu/stud/Model/FileHandler/TestFractals/AffineWriteTestFile");

      ChaosGameDescription descriptionFromFile =
          FileHandling.readFromFile(
              "src/test/java/edu/ntnu/stud/Model/FileHandler/TestFractals/AffineWriteTestFile");

      assertEquals(description, descriptionFromFile);

      FileHandling.deleteFile(
          "src/test/java/edu/ntnu/stud/Model/FileHandler/TestFractals/AffineWriteTestFile");
    }
  }

  /** Test if FileHandling writes JuliaTestFile correctly. */
  @Test
  @DisplayName("Test writeToFile method with JuliaTestFile.")
  void writeToFile_JuliaTestFile_ValidResult() throws Exception {
    List<Transform> transforms = new ArrayList<>();
    transforms.add(new JuliaTransform(new Complex(-0.74543, 0.11301), 1));
    transforms.add(new JuliaTransform(new Complex(-0.74543, 0.11301), -1));

    ChaosGameDescription description =
        new ChaosGameDescription(
            transforms, new Vector2D(new double[] {-1.6, -1}), new Vector2D(new double[] {1.6, 1}));

    FileHandling.writeToFile(
        description,
        "src/test/java/edu/ntnu/stud/Model/FileHandler/TestFractals/JuliaWriteTestFile");

    ChaosGameDescription descriptionFromFile =
        FileHandling.readFromFile(
            "src/test/java/edu/ntnu/stud/Model/FileHandler/TestFractals/JuliaWriteTestFile");

    assertEquals(description, descriptionFromFile);

    FileHandling.deleteFile(
        "src/test/java/edu/ntnu/stud/Model/FileHandler/TestFractals/JuliaWriteTestFile");
  }
}
