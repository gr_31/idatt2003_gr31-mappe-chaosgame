package edu.ntnu.stud.Model.Matrix;

import edu.ntnu.stud.Model.Vector.Vector;

/**
 * Interface for a matrix.
 */
public interface Matrix {
  Vector multiplyVector(Vector vector);

  double[][] getMatrix();
}
