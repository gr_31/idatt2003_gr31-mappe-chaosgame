package edu.ntnu.stud.View.GUIComponents;

import edu.ntnu.stud.Controller.ExceptionHandler;
import edu.ntnu.stud.Model.ChaosGameDescription.ChaosGameDescriptionType;
import edu.ntnu.stud.Controller.Observer;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

/**
 * Class for the navigation bar.
 *
 * <p>The purpose of this class is to create a navigation bar for the application.
 */
public class NavBar {
  List<Observer> observers = new ArrayList<>();
  private static NavBar single_instance = null;
  private final MenuBar navBar;
  private final List<String> fractalNames;
  private final List<ChaosGameDescriptionType> predefinedFractalTypes;
  private String iterations;

  /**
   * Constructor for NavBar.
   *
   * <p>The purpose of this constructor is to create a navigation bar instance for use with
   * singleton design pattern.
   */
  private NavBar() {
    navBar = new MenuBar();
    navBar.getStyleClass().add("nav-bar");

    iterations = "100000";

    fractalNames = new ArrayList<>();
    predefinedFractalTypes = new ArrayList<>();
    constructNavBar();

    StackPane container = new StackPane(navBar);
    container.getStyleClass().add("nav-bar-container");
  }

  /**
   * Add fractal options to the navigation bar.
   *
   * @param fractalName The names of the fractals.
   */
  public void addFractal(List<String> fractalName) {
    fractalNames.clear();
    fractalNames.addAll(fractalName);
    constructNavBar();
  }

  /**
   * Add fractal options to the navigation bar.
   */
  public void addFractalTypes(ChaosGameDescriptionType[] fractalType) {
    predefinedFractalTypes.clear();
    predefinedFractalTypes.addAll(List.of(fractalType));
    constructNavBar();
  }

  /**
   * Set the count of iterations.
   *
   * @param iterations The number of iterations.
   */
  public void setIterations(String iterations) {
    this.iterations = iterations;
    constructNavBar();
  }

  /**
   * Get the navigation bar.
   *
   * @return The navigation bar.
   */
  public MenuBar getNavBar() {
    return navBar;
  }

  /**
   * Construct the navigation bar.
   *
   * <p>The purpose of this method is to create the contents of the navigation bar.
   */
  private void constructNavBar() {
    Menu fractalMenu = generateSelectFractalMenu();
    Menu iterationsField = generateChangeIterationsField();

    navBar.getMenus().setAll(fractalMenu, iterationsField);
  }

  /**
   * Generate a label for the fractal menu.
   *
   * @param fractalName The name of the fractal.
   * @return The label for the fractal menu.
   */
  private Label generateSelectFractalMenuLabel(String fractalName) {
    Label fractalMenuItemLabel = new Label(fractalName);
    fractalMenuItemLabel.getStyleClass().add("fractal-menu-item");
    fractalMenuItemLabel.setMinWidth(150);
    fractalMenuItemLabel.setMaxWidth(150);
    fractalMenuItemLabel.wrapTextProperty().set(false);

    return fractalMenuItemLabel;
  }

  /**
   * Load an icon from a file.
   *
   * @param path The path to the icon.
   * @return The icon.
   */
  private ImageView loadIcon(String path) {
    try {
      FileInputStream inputStream = new FileInputStream(path);
      ImageView icon = new ImageView(new Image(inputStream));
      icon.setFitHeight(15);
      icon.setFitWidth(15);
      return icon;
    } catch (FileNotFoundException e) {
      ActionFeedback.showErrorDialogBox("Icon not found at: " + path);
      ExceptionHandler.getInstance().logException(e, "NavBar.loadIcon");
    }
    return null;
  }

  /**
   * Generate a button with an icon.
   *
   * @param path The path to the icon.
   * @return The button with the icon.
   */
  private Button generateButton(String path) {
    ImageView icon = loadIcon(path);
    Button button = new Button();
    button.setGraphic(icon);
    return button;
  }

  /**
   * Generate buttons to modify the fractal values.
   *
   * @param fractalName The name of the fractal.
   * @return The buttons to modify the fractal.
   */
  private List<Button> GenerateSelectFractalModifyButtons(String fractalName) {
    List<Button> buttons = new ArrayList<>();
    Button buttonD = generateButton("src/main/resources/Icons/trash_can.png");

    buttonD
        .onMouseClickedProperty()
        .set(
            event -> {
              notifyObservers("DeleteFractal", fractalName);
              fractalNames.remove(fractalName);
              constructNavBar();
            });


    buttons.add(buttonD);
    return buttons;
  }

  /**
   * Generate a row for the sub menu.
   *
   * @param fractalName       The name of the fractal.
   * @param menuItem          The menu item.
   * @param selectFractalMenu The menu to select the fractal.
   */
  private void generateFractalMenuRow(String fractalName, HBox menuItem, Menu selectFractalMenu) {
    CustomMenuItem fractalMenuItem = new CustomMenuItem(menuItem);

    selectFractalMenu.getItems().add(fractalMenuItem);

    fractalMenuItem.setOnAction(
        event -> {
          notifyObservers("Fractal", fractalName);
          selectFractalMenu.setText(fractalName);
        });
  }

  /**
   * Generate a menu row for the predefined fractal menu.
   *
   * @param fractalName          The name of the fractal.
   * @param fractalMenuItemLabel The label for the fractal menu item.
   * @param selectFractalMenu    The menu to select the fractal.
   */
  private void generatePredefinedFractalMenuRow(
      String fractalName, Label fractalMenuItemLabel, Menu selectFractalMenu) {
    CustomMenuItem fractalMenuItem = new CustomMenuItem(fractalMenuItemLabel);

    selectFractalMenu.getItems().add(fractalMenuItem);

    fractalMenuItem.setOnAction(
        event -> {
          notifyObservers("PredefinedFractal", fractalName);
          selectFractalMenu.setText(fractalName);
        });
  }

  /**
   * Generate a menu to fetch fractals from a folder.
   *
   * @return The menu to fetch fractals from a folder.
   */
  private MenuItem generateFetchFractals() {
    MenuItem fetchFractals = new MenuItem("Fetch Fractals from folder");
    fetchFractals.setOnAction(
        event -> notifyObservers("FetchFractals", null));
    return fetchFractals;
  }

  /**
   * Generate a menu item to create a new fractal.
   *
   * @return The menu item to create a new fractal.
   */
  private MenuItem generateCreateNewFractalButton() {
    MenuItem createNewFractal = new MenuItem("Create New Fractal");
    createNewFractal.setOnAction(
        event -> notifyObservers("CreateNewFractal", null));
    return createNewFractal;
  }

  /**
   * Generate menu to select a fractal.
   *
   * @return The menu to select a fractal.
   */
  private Menu generateSelectFractalMenu() {
    Menu selectFractalMenu = new Menu("Select Fractal");
    constructMenuItemsFromPredefinedFractals(selectFractalMenu);
    constructMenuItemsFromFiles(selectFractalMenu);

    MenuItem createNewFractal = generateCreateNewFractalButton();
    MenuItem fetchFractals = generateFetchFractals();

    selectFractalMenu.getItems().addAll(createNewFractal, fetchFractals);
    return selectFractalMenu;
  }

  /**
   * Construct menu items from predefined fractals.
   *
   * @param selectFractalMenu The menu to select the fractal.
   */
  private void constructMenuItemsFromPredefinedFractals(Menu selectFractalMenu) {
    predefinedFractalTypes.forEach(
        fractalType -> {
          Label fractalMenuItemLabel = generateSelectFractalMenuLabel(fractalType.toString());
          generatePredefinedFractalMenuRow(
              fractalType.toString(), fractalMenuItemLabel, selectFractalMenu);
        });
  }

  /**
   * Construct menu items from files.
   *
   * @param selectFractalMenu The menu to select the fractal.
   */
  private void constructMenuItemsFromFiles(Menu selectFractalMenu) {
    fractalNames.forEach(
        fractalName -> {
          Label fractalMenuItemLabel = generateSelectFractalMenuLabel(fractalName);
          List<Button> modifyButtons = GenerateSelectFractalModifyButtons(fractalName);

          HBox menuItem = new HBox();
          menuItem
              .getChildren()
              .addAll(fractalMenuItemLabel, modifyButtons.get(0));
          generateFractalMenuRow(fractalName, menuItem, selectFractalMenu);
        });
  }

  /**
   * Create a menu to change the count of iterations.
   *
   * @return The menu to change the count of iterations.
   */
  private Menu generateChangeIterationsField() {
    Menu changeIterationsMenu = new Menu("Change Iterations");

    TextField inputField = new TextField(String.valueOf(iterations));
    CustomMenuItem iterationsField = new CustomMenuItem(inputField);
    iterationsField.setHideOnClick(false);

    MenuItem applyButton = new MenuItem("Apply");
    applyButton.setOnAction(
        event -> {
          this.iterations = inputField.getText();

          notifyObservers("Iterations", inputField.getText());
        });

    changeIterationsMenu.getItems().addAll(iterationsField, applyButton);

    return changeIterationsMenu;
  }

  /**
   * Add an observer to the list of observers.
   *
   * @param observer The observer to add.
   */
  public void addObserver(Observer observer) {
    observers.add(observer);
  }

  /**
   * Remove an observer from the list of observers.
   *
   * @param observer The observer to remove.
   */
  public void removeObserver(Observer observer) {
    observers.remove(observer);
  }

  /**
   * Notify the observers of a change.
   *
   * @param changedValue The value that has changed.
   * @param value        The new value.
   */
  private void notifyObservers(String changedValue, Object value) {
    observers.forEach(observer -> observer.update(changedValue, value));
  }

  /**
   * Get the instance of the NavBar object.
   *
   * @return The instance of the navigation bar.
   */
  public static synchronized NavBar getInstance() {
    if (single_instance == null) {
      single_instance = new NavBar();
    }

    return single_instance;
  }
}
