package edu.ntnu.stud.Controller;

import edu.ntnu.stud.Model.ChaosGameDescription.ChaosGameDescription;
import edu.ntnu.stud.Model.FileHandling;
import edu.ntnu.stud.Model.Matrix.Matrix;
import edu.ntnu.stud.Model.Matrix.Matrix2D;
import edu.ntnu.stud.Model.Transformation.AffineTransform2D;
import edu.ntnu.stud.Model.Transformation.JuliaTransform;
import edu.ntnu.stud.Model.Transformation.Transform;
import edu.ntnu.stud.Model.Vector.Complex;
import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for storing fractals created by users.
 */
public class StoredFractalModifier {
  private List<Transform> transforms;
  private final List<Matrix> affineMatrices;
  private final List<Vector> affineVectors;
  private String filePath;

  /**
   * Constructs a StoredFractalModifier object, note filePath variable is hard coded.
   */
  public StoredFractalModifier() {
    this.transforms = new ArrayList<>();
    this.affineMatrices = new ArrayList<>();
    this.affineVectors = new ArrayList<>();
    this.filePath = "src/main/resources/Fractals/";
  }

  /**
   * Creates a Matrix2D object from array.
   *
   * @param matrixValuesString The values of the matrix.
   * @return A Matrix2D object.
   */
  private Matrix createMatrix2D(String[][] matrixValuesString) {
    double[][] matrixValues = new double[matrixValuesString.length][matrixValuesString[0].length];
    for (int i = 0; i < matrixValues.length; i++) {
      for (int j = 0; j < matrixValues[0].length; j++) {
        matrixValues[i][j] = Double.parseDouble(matrixValuesString[i][j]);
      }
    }

    return new Matrix2D(matrixValues);
  }

  /**
   * Creates a Vector2D object from array.
   *
   * @param vectorValuesString The values of the vector.
   * @return A Vector2D object.
   */
  private Vector createVector2D(String[] vectorValuesString) {
    double[] vectorValues = new double[vectorValuesString.length];
    for (int i = 0; i < vectorValuesString.length; i++) {
      vectorValues[i] = Double.parseDouble(vectorValuesString[i]);
    }

    return new Vector2D(vectorValues);
  }

  /**
   * Creates a Complex object from double values.
   *
   * @param complexValueRealString The real value of the complex number.
   * @param complexValueImString   The imaginary value of the complex number.
   * @return A Complex object.
   */
  private Complex createComplex(String complexValueRealString, String complexValueImString) {
    double complexValueReal = Double.parseDouble(complexValueRealString);
    double complexValueIm = Double.parseDouble(complexValueImString);

    return new Complex(complexValueReal, complexValueIm);
  }

  /**
   * Creates a list of JuliaTransform objects.
   *
   * @param complex The complex number.
   * @return A list of JuliaTransform objects.
   */
  private List<Transform> createJulia2dTransform(Complex complex) {
    return List.of(new JuliaTransform(complex, 1), new JuliaTransform(complex, -1));
  }

  /**
   * Creates an AffineTransform2D object.
   *
   * @param matrix The matrix of the affine transformation.
   * @param vector The vector of the affine transformation.
   * @return An AffineTransform2D object.
   */
  private Transform createAffine2dTransform(Matrix matrix, Vector vector) {
    return new AffineTransform2D(matrix, vector);
  }

  /**
   * Creates a ChaosGameDescription object.
   *
   * @param transforms The list of transformations.
   * @param minPoint   The minimum point.
   * @param maxPoint   The maximum point.
   * @return A ChaosGameDescription object.
   */
  private ChaosGameDescription createChaosGameDescription(
      List<Transform> transforms, Vector minPoint, Vector maxPoint) {
    return new ChaosGameDescription(transforms, minPoint, maxPoint);
  }

  /**
   * Converts components of julia transformation to ChaosGameDescription object, and stores it.
   *
   * @param complexValueReal The real value of the complex number.
   * @param complexValueIm   The imaginary value of the complex number.
   * @param minPoints        The minimum points.
   * @param maxPoints        The maximum points.
   * @throws IOException if an I/O error occurs.
   */
  public void storeJuliaFractal(
      String complexValueReal, String complexValueIm, String[] minPoints, String[] maxPoints)
      throws IOException {
    try {
      Complex complex = createComplex(complexValueReal, complexValueIm);
      this.transforms = createJulia2dTransform(complex);
      ChaosGameDescription chaosGameDescription =
          createChaosGameDescription(
              this.transforms, createVector2D(minPoints), createVector2D(maxPoints));
      FileHandling.writeToFile(chaosGameDescription, getFilePath());
    } catch (NumberFormatException e) {
      ExceptionHandler.getInstance().logException(e, "Invalid input while storing Julia fractal.");
      throw new IOException("Invalid input. Please enter a valid input.");
    }
  }

  /**
   * Converts components of affine transformation to ChaosGameDescription object, and stores it.
   *
   * @param matrices  The list of matrices.
   * @param vectors   The list of vectors.
   * @param minPoints The minimum points.
   * @param maxPoints The maximum points.
   * @throws IOException if an I/O error occurs.
   */
  public void storeAffineFractal(
      List<String[][]> matrices, List<String[]> vectors, String[] minPoints, String[] maxPoints)
      throws IOException {
    try {
      this.affineMatrices.clear();
      this.affineVectors.clear();
      this.transforms.clear();

      for (int i = 0; i < matrices.size(); i++) {
        this.affineMatrices.add(createMatrix2D(matrices.get(i)));
        this.affineVectors.add(createVector2D(vectors.get(i)));
        this.transforms.add(
            createAffine2dTransform(this.affineMatrices.get(i), this.affineVectors.get(i)));
      }
      ChaosGameDescription chaosGameDescription =
          createChaosGameDescription(
              this.transforms, createVector2D(minPoints), createVector2D(maxPoints));
      FileHandling.writeToFile(chaosGameDescription, getFilePath());
    } catch (NumberFormatException e) {
      ExceptionHandler.getInstance().logException(e, "Invalid input while storing affine fractal.");
      throw new IOException("Invalid input. Please enter a valid input.");
    }
  }

  /**
   * Sets the file path, to be default path or partial path.
   *
   * @param filePath The file path.
   */
  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  /**
   * Gets the file path.
   *
   * @return The file path.
   */
  public String getFilePath() {
    return this.filePath;
  }
}