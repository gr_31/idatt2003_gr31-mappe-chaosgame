package edu.ntnu.stud.Model.Vector;

/**
 * Class for representing a complex number.
 */
public class Complex extends Vector2D {

  /**
   * Constructs a Complex number with the given real and imaginary parts.
   *
   * @param real      The real part of the complex number.
   * @param imaginary The imaginary part of the complex number.
   */
  public Complex(double real, double imaginary) {
    super(new double[] {real, imaginary});
  }

  /**
   * Gets the real part of the complex number.
   *
   * @return The real part of the complex number.
   */
  public double getReal() {
    return super.getVector()[0];
  }

  /**
   * Gets the imaginary part of the complex number.
   *
   * @return The imaginary part of the complex number.
   */
  public double getImaginary() {
    return super.getVector()[1];
  }

  /**
   * Returns a string representation of the complex number in the form "a + bi".
   *
   * @return The string representation of the complex number.
   */
  public String displayComplexNumber() {
    return getReal() + " + " + getImaginary() + "i";
  }

  /**
   * Get the complex number as an array.
   *
   * @return An array representing the complex number.
   */
  public double[] getComplex() {
    return super.getVector();
  }

  /**
   * Adds another vector to this complex number.
   *
   * @param otherVector The vector to be added.
   * @return A new Complex number representing the sum of this and the other vector.
   * @throws IllegalArgumentException If the other vector is not an instance of Complex
   * @throws IllegalArgumentException If the other vector is not 2D.
   */
  @Override
  public Vector add(Vector otherVector) {
    if (!(otherVector instanceof Complex)) {
      throw new IllegalArgumentException(
          "Cannot add complex number and vector with 2 real numbers");
    }
    if (otherVector.getVector().length != 2) {
      throw new IllegalArgumentException("Vector must be 2D");
    }
    double resultX1 = this.getVector()[0] + otherVector.getVector()[0];
    double resultX2 = this.getVector()[1] + otherVector.getVector()[1];

    return new Complex(resultX1, resultX2);
  }

  /**
   * Subtracts another vector from this complex number.
   *
   * @param otherVector The vector to be subtracted.
   * @return A new Complex number representing the difference of this and the other vector.
   * @throws IllegalArgumentException If the other vector is not an instance of Complex
   * @throws IllegalArgumentException If the other vector is not 2D.
   */
  @Override
  public Vector subtract(Vector otherVector) {
    if (!(otherVector instanceof Complex)) {
      throw new IllegalArgumentException(
          "Cannot add complex number and vector with 2 real numbers");
    }
    if (otherVector.getVector().length != 2) {
      throw new IllegalArgumentException("Vector must be 2D");
    }

    double resultX1 = this.getVector()[0] - otherVector.getVector()[0];
    double resultX2 = this.getVector()[1] - otherVector.getVector()[1];

    return new Complex(resultX1, resultX2);
  }

  /**
   * Computes the square root of the complex number.
   *
   * @return A new Complex number representing the square root of this complex number.
   */
  public Complex sqrt() {
    double newReal =
        Math.sqrt(
            (Math.sqrt(getReal() * getReal() + getImaginary() * getImaginary()) + getReal()) / 2);
    double newImaginary =
        Math.signum(getImaginary())
            * Math.sqrt(
            (Math.sqrt(Math.pow(getReal(), 2) + Math.pow(getImaginary(), 2)) - getReal()) / 2);
    return new Complex(newReal, newImaginary);
  }

  /**
   * Checks if two complex numbers are equal.
   *
   * @param obj The object to compare with this complex number.
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (!(obj instanceof Complex other)) {
      return false;
    }

    return getReal() == other.getReal() && getImaginary() == other.getImaginary();
  }
}
