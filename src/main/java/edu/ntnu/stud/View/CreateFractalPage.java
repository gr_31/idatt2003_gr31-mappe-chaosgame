package edu.ntnu.stud.View;


import edu.ntnu.stud.Controller.ExceptionHandler;
import edu.ntnu.stud.Controller.SceneNavigation;
import edu.ntnu.stud.Controller.StoredFractalModifier;
import edu.ntnu.stud.View.GUIComponents.ActionFeedback;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * Class for creating the creating fractal page.
 */
public class CreateFractalPage {
  private Scene createFractalScene;
  private final SceneNavigation sceneNavigation;
  private TabPane tabPane;
  private StoredFractalModifier storedFractalModifier;
  private TextField filePathTextField;
  private VBox affineSection;
  private HBox juliaSection;
  private VBox pageContent;

  /**
   * Constructor for the create fractal page.
   *
   * @param sceneNavigation The scene navigation object responsible for navigating between pages.
   */
  public CreateFractalPage(SceneNavigation sceneNavigation) {
    this.sceneNavigation = sceneNavigation;
    try {
      this.storedFractalModifier = new StoredFractalModifier();
      this.pageContent = constructScene();
      BorderPane root = new BorderPane();
      root.setCenter(this.pageContent);
      root.setBackground(Background.fill(Color.BLACK));
      this.createFractalScene = new Scene(root);
      this.createFractalScene.getStylesheets().add("/Stylesheets/stylesCreateFractal.css");
    } catch (Exception e) {
      ExceptionHandler.getInstance().logException(e, "Create fractal page failed to initialize.");
      ActionFeedback.showErrorDialogBox("Initialization error: " + e.getMessage());
    }
  }

  /**
   * Method to get: createFractalScene, used to navigate to the
   * fractal page.
   *
   * @return The created  fractal scene.
   */
  public Scene getCreateStoredFractalScene() {
    return this.createFractalScene;
  }

  /**
   * Method to construct and get the create fractal page content.
   *
   * @return The create fractal page content.
   */
  private VBox constructScene() {
    HBox header = createHeader();
    this.filePathTextField = constructFilePathInput();
    HBox minMaxSection = createMinMaxSection();
    this.tabPane = createTransformationTabs();
    addAffineSection();
    VBox pageContainer = new VBox();
    pageContainer.getChildren().addAll(header, this.filePathTextField, minMaxSection, this.tabPane);
    pageContainer.setAlignment(Pos.CENTER);
    pageContainer.setSpacing(10);
    return pageContainer;
  }

  /**
   * Method to get the minimum point values from the input boxes.
   *
   * @return The minimum point.
   */
  private TextField[] getMinPoint() {
    HBox minMaxBox = (HBox) this.pageContent.getChildren().get(2);
    return new TextField[] {
        ((TextField) minMaxBox.getChildren().get(2)), // Min X
        ((TextField) minMaxBox.getChildren().get(3)) // Min Y
    };
  }

  /**
   * Method to get the maximum point values from the input boxes.
   *
   * @return The maximum point.
   */
  private TextField[] getMaxPoint() {
    HBox minMaxBox = (HBox) this.pageContent.getChildren().get(2);
    return new TextField[] {
        ((TextField) minMaxBox.getChildren().get(0)), // Max X
        ((TextField) minMaxBox.getChildren().get(1)) // Max Y
    };
  }

  /**
   * Method to get the affine matrix text fields.
   */
  private TextField[] getAffineMatrixTextFields(int index) {
    VBox affineBox = (VBox) this.affineSection.getChildren().get(index);
    HBox matrixAndVector = (HBox) affineBox.getChildren().get(0);
    HBox matrixBox = (HBox) matrixAndVector.getChildren().get(0);
    VBox matrixFieldsBox = (VBox) matrixBox.getChildren().get(1);
    TextField x1 = (TextField) ((HBox) matrixFieldsBox.getChildren().get(0)).getChildren().get(0);
    TextField y1 = (TextField) ((HBox) matrixFieldsBox.getChildren().get(0)).getChildren().get(1);
    TextField x2 = (TextField) ((HBox) matrixFieldsBox.getChildren().get(1)).getChildren().get(0);
    TextField y2 = (TextField) ((HBox) matrixFieldsBox.getChildren().get(1)).getChildren().get(1);
    return new TextField[] {x1, y1, x2, y2};
  }

  /**
   * Method to get the affine vector text fields.
   */
  private TextField[] getAffineVectorTextFields(int index) {
    VBox affineBox = (VBox) this.affineSection.getChildren().get(index);
    HBox matrixAndVector = (HBox) affineBox.getChildren().get(0);
    HBox vectorBox = (HBox) matrixAndVector.getChildren().get(1);
    VBox vectorFieldsBox = (VBox) vectorBox.getChildren().get(1);
    TextField a = (TextField) vectorFieldsBox.getChildren().get(0);
    TextField b = (TextField) vectorFieldsBox.getChildren().get(1);
    return new TextField[] {a, b};
  }

  /**
   * Method to store the Julia input values as a file.
   */
  private void storeJuliaInput() throws IOException {
    TextField[] minPointTextField = getMinPoint();
    TextField[] maxPointTextField = getMaxPoint();
    String[] minPoint =
        new String[] {
            minPointTextField[0].getText(),
            minPointTextField[1].getText()
        };
    String[] maxPoint =
        new String[] {
            maxPointTextField[0].getText(),
            maxPointTextField[1].getText()
        };
    String realPart =
        ((TextField) this.juliaSection.getChildren().get(2)).getText();
    String imaginaryPart =
        ((TextField) this.juliaSection.getChildren().get(4)).getText();

    storedFractalModifier.setFilePath(filePathTextField.getText());
    storedFractalModifier.storeJuliaFractal(realPart, imaginaryPart, minPoint, maxPoint);
  }

  /**
   * Method to store the affine input values as a file.
   */
  private void storeAffineInput() throws IOException {
    List<String[][]> matrices = new ArrayList<>();
    List<String[]> vectors = new ArrayList<>();
    TextField[] minPointTextField = getMinPoint();
    TextField[] maxPointTextField = getMaxPoint();
    String[] minPoint =
        new String[] {
            minPointTextField[0].getText(),
            minPointTextField[1].getText()
        };
    String[] maxPoint =
        new String[] {
            maxPointTextField[0].getText(),
            maxPointTextField[1].getText()
        };
    for (int i = 0; i < (this.affineSection.getChildren().size() - 1); i++) {
      TextField[] matrixTextFields = getAffineMatrixTextFields(i);
      TextField[] vectorTextFields = getAffineVectorTextFields(i);
      matrices.add(
          new String[][] {
              {
                  matrixTextFields[0].getText(),
                  matrixTextFields[1].getText()
              },
              {
                  matrixTextFields[2].getText(),
                  matrixTextFields[3].getText()
              }
          });
      vectors.add(
          new String[] {
              vectorTextFields[0].getText(),
              vectorTextFields[1].getText()
          });
    }
    storedFractalModifier.setFilePath(filePathTextField.getText());
    storedFractalModifier.storeAffineFractal(matrices, vectors, minPoint, maxPoint);
  }

  /**
   * Method to create the header section of the create fractal page.
   *
   * @return The header section.
   */
  private HBox createHeader() {
    Button navBack = createNavBackButton();
    Label headlineLabel = new Label("Create fractal");
    headlineLabel.getStyleClass().add("headline-label");
    Button createButton = createApplyButton();
    HBox headSection = new HBox();
    headSection.getChildren().addAll(navBack, headlineLabel, createButton);
    headSection.setAlignment(Pos.CENTER);
    headSection.setSpacing(10);
    return headSection;
  }

  /**
   * Method to create the transformation tabs for affine transform fractals, and Julia transform
   * fractals.
   *
   * @return The transformation tab pane.
   */
  private TabPane createTransformationTabs() {
    Tab affineTab = createAffineTab();
    Tab juliaTab = createJuliaTab();
    TabPane tabPane = new TabPane();
    tabPane.getStyleClass().add("styled-tab-pane");
    tabPane.getTabs().addAll(affineTab, juliaTab);
    tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
    tabPane.setMaxWidth(600);
    tabPane.setPrefHeight(400);
    tabPane.setMaxHeight(400);
    return tabPane;
  }

  /**
   * Method to create the navigation back button.
   *
   * @return The navigation back button.
   */
  private Button createNavBackButton() {
    Button returnButton = new Button();
    returnButton.setText("Return");
    returnButton.setId("return-button");
    returnButton.setOnAction(
        e -> {
          if (ActionFeedback.showConfirmationDialogBox("Are you sure you want to return?")) {
            this.sceneNavigation.navigateToMainPage();
          }
        });
    return returnButton;
  }

  /**
   * Method to create the apply button with observer.
   *
   * @return The apply button.
   */
  private Button createApplyButton() {
    Button applyValues = new Button("Create");
    applyValues.setId("applyValuesButton");
    applyValues.getStyleClass().add("header-button");
    applyValues.setOnAction(
        event -> {
          if (this.filePathTextField.getText().isEmpty()) {
            ExceptionHandler.getInstance()
                .logException(new IllegalArgumentException("No file path inserted"),
                    "No file path inserted create fractal page");
            ActionFeedback.showErrorDialogBox("Please enter a file path");
            return;
          }

          try {
            if (this.tabPane.getSelectionModel().getSelectedIndex() == 1) {
              storeJuliaInput();
            } else {
              storeAffineInput();
            }
          } catch (Exception e) {
            ActionFeedback.showErrorDialogBox("Error: " + e.getMessage());
            ExceptionHandler.getInstance().logException(e, "Error while storing fractal");
            return;
          }
          ActionFeedback.showInformationDialogBox("Fractal created successfully!");
        });
    return applyValues;
  }

  /**
   * Method to construct the file path input field.
   *
   * @return The file path input field.
   */
  private TextField constructFilePathInput() {
    TextField filePathTextField = new TextField();
    filePathTextField.setText(storedFractalModifier.getFilePath());
    filePathTextField.setAlignment(Pos.CENTER);
    filePathTextField.setMaxWidth(450);
    return filePathTextField;
  }

  /**
   * Method to create the affine transformation tab.
   *
   * @return The affine transformation tab.
   */
  private Tab createAffineTab() {
    this.affineSection = new VBox();
    this.affineSection.getStyleClass().add("transform-section");
    this.affineSection.setAlignment(Pos.CENTER);
    Button addNewTransformationButton = new Button("Add new transformation");
    addNewTransformationButton.setOnAction(
        event -> addAffineSection());
    this.affineSection.getChildren().add(addNewTransformationButton);
    ScrollPane affineScrollPane = new ScrollPane(affineSection);
    affineScrollPane.setFitToWidth(true);
    affineScrollPane.setFitToHeight(true);
    Tab affineTab = new Tab("Affine transformation");
    affineTab.setClosable(false);
    affineTab.setContent(affineScrollPane);
    return affineTab;
  }

  /**
   * Method to create the Julia transformation tab.
   *
   * @return The Julia transformation tab.
   */
  private Tab createJuliaTab() {
    Tab juliaTab = new Tab("Julia transformation");
    juliaTab.setClosable(false);
    this.juliaSection = createCompleteJuliaSection();
    juliaTab.setContent(this.juliaSection);
    return juliaTab;
  }

  /**
   * Method to add a new affine section to the affine transformation tab.
   */
  private void addAffineSection() {
    int penultimateIndex = this.affineSection.getChildren().size() - 1;
    if (penultimateIndex < 0) {
      penultimateIndex = 0;
    }
    this.affineSection.getChildren().add(penultimateIndex, createNewAffineSection());
  }

  /**
   * Method to create a new affine section.
   *
   * @return The new affine section.
   */
  private VBox createNewAffineSection() {
    VBox affineSection = new VBox();
    HBox matrixAndVector = getMatrixAndVectorSection();
    Button removeButton = new Button("Remove");
    removeButton.setOnAction(
        e -> this.affineSection.getChildren().remove(affineSection));
    affineSection.getChildren().addAll(matrixAndVector, removeButton);
    affineSection.getStyleClass().add("affine-section");
    affineSection.setAlignment(Pos.CENTER);
    VBox.setMargin(affineSection, new Insets(0, 0, 0, 0));
    affineSection.setPadding(new Insets(10, 5, 30, 5));
    return affineSection;
  }

  /**
   * Method to create a new container with matrix inputs.
   *
   * @return The new matrix section.
   */
  private VBox createMatrixNoBrackets() {
    TextField textField1 = new TextField();
    TextField textField2 = new TextField();
    TextField textField3 = new TextField();
    TextField textField4 = new TextField();
    textField1.setPromptText("x1 y1");
    textField2.setPromptText("x1 y2");
    textField3.setPromptText("x2 y1");
    textField4.setPromptText("x2 y2");
    HBox row1 = new HBox(10, textField1, textField2);
    HBox row2 = new HBox(10, textField3, textField4);
    VBox matrixVbox = new VBox(10, row1, row2);
    matrixVbox.setAlignment(Pos.CENTER);
    return matrixVbox;
  }

  /**
   * Method to create a new matrix section with matrix inputs and brackets.
   *
   * @return The new matrix section.
   */
  private HBox createMatrixSection() {
    ImageView leftBracket = loadImageFromFile("src/main/resources/images/left-bracket.png", 20, 80);
    ImageView rightBracket =
        loadImageFromFile("src/main/resources/images/right-bracket.png", 20, 80);
    VBox matrix = createMatrixNoBrackets();
    HBox matrixWithBrackets = new HBox();
    matrixWithBrackets.getChildren().addAll(leftBracket, matrix, rightBracket);
    matrixWithBrackets.setAlignment(Pos.CENTER);
    return matrixWithBrackets;
  }

  /**
   * Method to create a new vector section with vector inputs and no brackets.
   *
   * @return The new vector section without brackets.
   */
  private VBox createVectorNoBrackets() {
    TextField aValue = new TextField();
    aValue.setPromptText("a");
    TextField bValue = new TextField();
    bValue.setPromptText("b");
    VBox vector = new VBox(10, aValue, bValue);
    vector.setAlignment(Pos.CENTER);
    return vector;
  }

  /**
   * Method to create a new vector section with vector inputs and brackets.
   *
   * @return The new vector section with brackets.
   */
  private HBox createVectorSection() {
    VBox vectorNoBrackets = createVectorNoBrackets();
    ImageView leftBracket = loadImageFromFile("src/main/resources/images/left-bracket.png", 20, 80);
    ImageView rightBracket =
        loadImageFromFile("src/main/resources/images/right-bracket.png", 20, 80);
    HBox vectorSection = new HBox();
    vectorSection.getChildren().addAll(leftBracket, vectorNoBrackets, rightBracket);
    vectorSection.setAlignment(Pos.CENTER);
    return vectorSection;
  }

  /**
   * Method to create a new matrix and vector section with brackets.
   *
   * @return The new matrix and vector section.
   */
  private HBox getMatrixAndVectorSection() {
    HBox matrixSection = createMatrixSection();
    HBox vectorSection = createVectorSection();
    HBox matrixAndVector = new HBox();
    matrixAndVector.getChildren().addAll(matrixSection, vectorSection);
    matrixAndVector.setAlignment(Pos.CENTER);
    return matrixAndVector;
  }

  /**
   * Method to create a new min or max point input field.
   *
   * @param promptText The prompt text for the input field.
   * @return The new min max input field.
   */
  private TextField getMinMaxField(String promptText) {
    TextField input = new TextField();
    input.setPromptText(promptText);
    input.setPrefWidth(50);
    input.setMaxWidth(50);
    input.setAlignment(Pos.CENTER);
    return input;
  }

  /**
   * Method to create a new min max section.
   *
   * @return The new min max section.
   */
  private HBox createMinMaxSection() {
    TextField maxX = getMinMaxField("Max X");
    TextField maxY = getMinMaxField("Max Y");
    TextField minX = getMinMaxField("Min X");
    TextField minY = getMinMaxField("Min Y");
    HBox minMaxSection = new HBox(maxX, maxY, minX, minY);
    minMaxSection.setAlignment(Pos.CENTER);
    minMaxSection.setSpacing(10);
    return minMaxSection;
  }

  /**
   * Method to create a complete Julia section.
   *
   * @return The complete Julia section.
   */
  private HBox createCompleteJuliaSection() {
    Label zLabel = new Label("Z");
    zLabel.getStyleClass().addAll("julia-z-label", "julia-label");
    TextField realPart = new TextField();
    realPart.setPromptText("Real part");
    realPart.setAlignment(Pos.CENTER);
    Label plus = new Label("+");
    plus.getStyleClass().add("julia-label");
    TextField imaginaryPart = new TextField();
    imaginaryPart.setPromptText("Imaginary part");
    imaginaryPart.setAlignment(Pos.CENTER);
    Label imaginaryI = new Label("i");
    imaginaryI.getStyleClass().addAll("julia-label");
    HBox juliaSection = new HBox();
    ImageView arrow = loadImageFromFile("src/main/resources/images/arrow.png", 20, 30);
    juliaSection.getChildren().addAll(zLabel, arrow, realPart, plus, imaginaryPart, imaginaryI);
    juliaSection.setAlignment(Pos.CENTER);
    juliaSection.setSpacing(6);
    juliaSection.getStyleClass().add("transform-section");
    return juliaSection;
  }

  /**
   * Method to load an image from a file path.
   *
   * @param filePath The file path to the image.
   * @param height   The height of the image.
   * @param width    The width of the image.
   * @return The image.
   */
  private ImageView loadImageFromFile(String filePath, int height, int width) {
    try {
      FileInputStream inputStream = new FileInputStream(filePath);
      Image image = new Image(inputStream, height, width, false, false);
      ImageView imageView = new ImageView(image);
      imageView.setPreserveRatio(true);
      return imageView;
    } catch (FileNotFoundException e) {
      ActionFeedback.showErrorDialogBox("Could not find file with path: " + filePath);
      ExceptionHandler.getInstance().logException(e, "Error loading image created fractal page");
      return new ImageView();
    }
  }
}

