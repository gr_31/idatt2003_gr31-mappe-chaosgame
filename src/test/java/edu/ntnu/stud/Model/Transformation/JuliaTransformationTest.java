package edu.ntnu.stud.Model.Transformation;

import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;
import edu.ntnu.stud.Model.Vector.Complex;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * JUnit tests for the JuliaTransformation class, covering positive and negative scenarios.
 */
public class JuliaTransformationTest {
  /**
   * Positive test cases for JuliaTransform.
   */
  @Nested
  @DisplayName("Positive tests for JuliaTransform class.")
  class PositiveTests {

    @Test
    @DisplayName("Test transform method with positive sign.")
    void transform_PositiveSign_ValidResult() {
      JuliaTransform juliaTransform = new JuliaTransform(new Complex(1.0, 1.0), 1);
      Vector vectorZ = new Vector2D(new double[] {2.0, 2.0});
      Vector result = juliaTransform.transform(vectorZ);
      assertArrayEquals(new double[] {1.09868411346781, 0.4550898605622274}, result.getVector(),
          1e-10);
    }

    @Test
    @DisplayName("Test transform method with negative sign.")
    void transform_NegativeSign_ValidResult() {
      JuliaTransform juliaTransform = new JuliaTransform(new Complex(1.0, 1.0), -1);
      Vector vectorZ = new Vector2D(new double[] {2.0, 2.0});
      Vector result = juliaTransform.transform(vectorZ);
      assertArrayEquals(new double[] {-1.09868411346781, -0.4550898605622274}, result.getVector(),
          1e-10);
    }

    @Test
    @DisplayName("Test equals method with same instance.")
    void equals_sameInstance_returnTrue() {
      JuliaTransform juliaTransform = new JuliaTransform(new Complex(1.0, 1.0), 1);
      assertEquals(juliaTransform, juliaTransform);
    }

    @Test
    @DisplayName("Test equals method with different instances, same values.")
    void equals_differentInstancesSameValues_returnTrue() {
      JuliaTransform juliaTransform1 = new JuliaTransform(new Complex(1.0, 1.0), 1);
      JuliaTransform juliaTransform2 = new JuliaTransform(new Complex(1.0, 1.0), 1);
      assertEquals(juliaTransform1, juliaTransform2);
    }

    @Test
    @DisplayName("Test getPoint method returns correct point.")
    void getPoint_validPoint_returnCorrectPoint() {
      Complex complex = new Complex(1.0, 2.0);
      JuliaTransform juliaTransform = new JuliaTransform(complex, 1);
      assertEquals(complex, juliaTransform.getPoint());
    }
  }

  /**
   * Negative test cases for JuliaTransform.
   */
  @Nested
  @DisplayName("Negative tests for JuliaTransform class.")
  class NegativeTests {

    @Test
    @DisplayName("Test transform method with invalid sign.")
    void transform_InvalidSign_ThrowsIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class,
          () -> new JuliaTransform(new Complex(1.0, 1.0), 0));
    }

    @Test
    @DisplayName("Test transform method with null point.")
    void transform_NullPoint_ThrowsIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> new JuliaTransform(null, 1));
    }

    @Test
    @DisplayName("Test transform method with positive sign and invalid result.")
    void transform_PositiveSign_InvalidResult() {
      JuliaTransform juliaTransform = new JuliaTransform(new Complex(1.0, 1.0), 1);
      Vector vectorZ = new Vector2D(new double[] {2.0, 2.0});
      Vector result = juliaTransform.transform(vectorZ);
      assertNotEquals(new double[] {3, 2}, result.getVector());
    }

    @Test
    @DisplayName("Test transform method with negative sign and invalid result.")
    void transform_NegativeSign_InvalidResult() {
      JuliaTransform juliaTransform = new JuliaTransform(new Complex(1.0, 1.0), -1);
      Vector vectorZ = new Vector2D(new double[] {2.0, 2.0});
      Vector result = juliaTransform.transform(vectorZ);
      assertNotEquals(new double[] {-2, 3}, result.getVector());
    }

    @Test
    @DisplayName("Test equals method with different instances, different values.")
    void equals_differentInstancesDifferentValues_returnFalse() {
      JuliaTransform juliaTransform1 = new JuliaTransform(new Complex(1.0, 1.0), 1);
      JuliaTransform juliaTransform2 = new JuliaTransform(new Complex(2.0, 2.0), -1);
      assertNotEquals(juliaTransform1, juliaTransform2);
    }

    @Test
    @DisplayName("Test equals method with different types.")
    void equals_differentTypes_returnFalse() {
      JuliaTransform juliaTransform = new JuliaTransform(new Complex(1.0, 1.0), 1);
      Object differentObject = new Object();
      assertNotEquals(juliaTransform, differentObject);
    }
  }
}
