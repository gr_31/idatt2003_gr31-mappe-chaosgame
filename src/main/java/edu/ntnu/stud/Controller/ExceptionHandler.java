package edu.ntnu.stud.Controller;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for handling exceptions in the application.
 */
public class ExceptionHandler {
  private static final Logger exceptionLogger = Logger.getLogger("ExceptionHandler");
  private static ExceptionHandler instance;

  /**
   * Constructor for ExceptionHandler.
   */
  private ExceptionHandler() {}

  /**
   * Method to get the instance of the ExceptionHandler.
   *
   * @return the instance of the ExceptionHandler
   */
  public static ExceptionHandler getInstance() {
    if (instance == null) {
      instance = new ExceptionHandler();
    }
    return instance;
  }

  /**
   * Method to log an exception.
   *
   * @param e the exception to log
   */
  public void logException(Exception e, String message) {
    exceptionLogger.log(Level.SEVERE, message + " " + e.getMessage(), e);
  }
}