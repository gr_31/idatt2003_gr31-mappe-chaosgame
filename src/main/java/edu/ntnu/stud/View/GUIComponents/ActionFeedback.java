package edu.ntnu.stud.View.GUIComponents;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 * Class for displaying feedback messages in the application.
 */
public class ActionFeedback {
  /**
   * Method to show an error dialog box with a message.
   *
   * @param error the error message to display
   */
  public static void showErrorDialogBox(String error) {
    Alert errorDialogBox = new Alert(Alert.AlertType.ERROR);
    errorDialogBox.setTitle("Error");
    errorDialogBox.setHeaderText("An error has occurred:");
    errorDialogBox.setContentText(error);

    errorDialogBox.showAndWait();
  }

  /**
   * Method to show a confirmation dialog box with a message, so application components can get
   * confirmation from user.
   *
   * @param confirmationPrompt the information message to display
   */
  public static boolean showConfirmationDialogBox(String confirmationPrompt) {
    Alert confirmationDialogBox = new Alert(Alert.AlertType.CONFIRMATION);
    confirmationDialogBox.setTitle("Confirmation");
    confirmationDialogBox.setHeaderText("Confirmation:");
    confirmationDialogBox.setContentText(confirmationPrompt);

    Optional<ButtonType> result = confirmationDialogBox.showAndWait();
    return result.isPresent() && result.get() == ButtonType.OK;
  }

  /**
   * Method to show an information dialog box with a message.
   *
   * @param information the information message to display
   */
  public static void showInformationDialogBox(String information) {
    Alert informationDialogBox = new Alert(Alert.AlertType.INFORMATION);
    informationDialogBox.setTitle("Information");
    informationDialogBox.setHeaderText("Information:");
    informationDialogBox.setContentText(information);

    informationDialogBox.showAndWait();
  }
}
