package edu.ntnu.stud.Controller;

import edu.ntnu.stud.View.CreateFractalPage;
import edu.ntnu.stud.View.GUIComponents.NavBar;
import edu.ntnu.stud.View.MainPage;
import edu.ntnu.stud.View.SceneHandler;
import javafx.scene.Scene;

/**
 * Scene navigation for the application.
 */
public class SceneNavigation implements Observer {
  private final SceneHandler sceneHandler;
  private final MainPage mainPage;
  private final CreateFractalPage createFractalPage;
  private int currentHeight;
  private int currentWidth;

  /**
   * Constructor for SceneNavigation.
   *
   * @param sceneHandler object of type SceneHandler
   */
  public SceneNavigation(SceneHandler sceneHandler) {
    this.sceneHandler = sceneHandler;
    this.currentWidth = 1000;
    this.currentHeight = 700;
    this.mainPage = new MainPage(this, currentHeight, currentWidth);
    this.createFractalPage = new CreateFractalPage(this);
    NavBar.getInstance().addObserver(this);
    navigateToMainPage();
  }

  /**
   * Resize the page.
   *
   * @param height the height of the page
   * @param width  the width of the page
   */
  public void pageResize(int height, int width) {
    this.currentHeight = height;
    this.currentWidth = width;
    this.mainPage.windowResize(height, width);
  }

  /**
   * Navigate to the main page.
   */
  public void navigateToMainPage() {
    this.changeScene(this.mainPage.getMainPageScene());
  }

  /**
   * Navigate to the create fractal page.
   */
  public void navigateToCreateFractalPage() {
    this.changeScene(this.createFractalPage.getCreateStoredFractalScene());
  }

  /**
   * Switch scene in scene handler.
   */
  private void changeScene(Scene scene) {
    this.sceneHandler.changeScene(scene, currentHeight, currentWidth);
  }

  /**
   * Navigate to navigateToModifyStoredFractalPage on observer update.
   *
   * @param changedValueName the name of the changed value
   * @param newValue         the path to the fractal on edit
   */
  @Override
  public void update(String changedValueName, Object newValue) {
    if (changedValueName.equals("CreateNewFractal")) {
      this.navigateToCreateFractalPage();
    }
  }
}
