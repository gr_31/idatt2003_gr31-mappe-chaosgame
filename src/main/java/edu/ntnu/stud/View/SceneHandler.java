package edu.ntnu.stud.View;

import edu.ntnu.stud.Controller.SceneNavigation;
import javafx.animation.PauseTransition;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Scene handler for the application.
 */
public class SceneHandler extends Application {
  Stage primaryStage;

  /**
   * Change the scene of the primary stage.
   *
   * @param scene the scene to change to
   */
  public void changeScene(Scene scene, int height, int width) {
    this.primaryStage.setScene(scene);
    this.primaryStage.setHeight(height);
    this.primaryStage.setWidth(width);
    this.primaryStage.show();
  }

  /**
   * Start method for the application.
   *
   * @param primaryStage the primary stage for the GUI
   */
  @Override
  public void start(Stage primaryStage) {
    primaryStage.setTitle("Chaos game");
    primaryStage.setHeight(700);
    primaryStage.setWidth(1000);
    this.primaryStage = primaryStage;

    SceneNavigation sceneNavigation = new SceneNavigation(this);

    PauseTransition pauseAfterResize = new PauseTransition(Duration.millis(200));
    pauseAfterResize.setOnFinished(
        event -> sceneNavigation.pageResize(
            primaryStage.heightProperty().intValue(), primaryStage.widthProperty().intValue()));

    primaryStage
        .widthProperty()
        .addListener(
            (obs, oldVal, newVal) -> pauseAfterResize.playFromStart());

    primaryStage
        .heightProperty()
        .addListener(
            (obs, oldVal, newVal) -> pauseAfterResize.playFromStart());

    primaryStage.show();
  }
}