package edu.ntnu.stud;

import edu.ntnu.stud.View.SceneHandler;

/**
 * Main class for the Chaos Game application. Launches the application.
 */
public class ChaosGameApp {
  /**
   * Launches the application.
   *
   * @param args The command line arguments.
   */
  public static void main(String[] args) {
    SceneHandler.launch(SceneHandler.class, args);
  }
}
