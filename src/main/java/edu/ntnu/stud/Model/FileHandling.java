package edu.ntnu.stud.Model;

import edu.ntnu.stud.Model.ChaosGameDescription.ChaosGameDescription;
import edu.ntnu.stud.Model.Matrix.Matrix;
import edu.ntnu.stud.Model.Matrix.Matrix2D;
import edu.ntnu.stud.Model.Transformation.AffineTransform2D;
import edu.ntnu.stud.Model.Transformation.JuliaTransform;
import edu.ntnu.stud.Model.Transformation.Transform;
import edu.ntnu.stud.Model.Vector.Complex;
import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class for handling configuration file for chaos game.
 *
 * <p>The purpose of this class is to read and write to configuration files.
 */
public class FileHandling {
  /**
   * Reads a file and removes comments from the line, and removes spaces at the beginning and end of
   * line.
   *
   * @param reader The BufferedReader to read from.
   * @return A list of strings, where each string is a line from the file.
   * @throws java.io.IOException if an I/O error occurs.
   */
  private static List<String> getDataLines(BufferedReader reader) throws java.io.IOException {
    List<String> dataLines = new ArrayList<>();
    String line;

    while ((line = reader.readLine()) != null) {
      dataLines.add(removeComments(line));
    }

    return dataLines;
  }

  /**
   * Removes comments and spacing in front of and after the contents of a line.
   *
   * @param line The line to remove comments from.
   * @return The line without comments.
   */
  private static String removeComments(String line) {
    return line.split("#")[0].trim();
  }

  /**
   * Parses a string of doubles separated by commas.
   *
   * @param line The line to parse.
   * @return A list of doubles.
   * @throws NumberFormatException if input value is not a valid double.
   */
  private static List<Double> parseDoubles(String line) {
    try {
      return Arrays.stream(line.split(",")).map(Double::parseDouble).collect(Collectors.toList());
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Error parsing doubles from line: " + line, e);
    }
  }

  /**
   * Creates 2D vector from list of doubles.
   *
   * @param doubles The list of doubles to create the vector from.
   * @return A 2D vector.
   */
  private static Vector readVector2D(List<Double> doubles) throws IllegalArgumentException {
    if (doubles.size() != 2) {
      throw new IllegalArgumentException("Vector must have 2 components");
    }
    return new Vector2D(new double[] {doubles.get(0), doubles.get(1)});
  }

  /**
   * Creates 2D matrix from list of doubles.
   *
   * @param doubles The list of doubles to create the matrix from.
   * @return A 2D matrix.
   */
  private static Matrix readMatrix2D(List<Double> doubles) throws IllegalArgumentException {
    if (doubles.size() != 4) {
      throw new IllegalArgumentException("Matrix 2D must have 4 components");
    }
    return new Matrix2D(
        new double[][] {{doubles.get(0), doubles.get(1)}, {doubles.get(2), doubles.get(3)}});
  }

  /**
   * Reads an affine transformation from a line.
   *
   * @param line The line to read from.
   * @return An affine transformation.
   */
  private static Transform readAffineTransform2D(String line) {
    List<Double> doubles = parseDoubles(line);

    if (doubles.size() != 6) {
      throw new IllegalArgumentException(
          "Affine transform must have a matrix with 4 components, and a vector with 2 components");
    }

    Matrix transformMatrix = readMatrix2D(doubles.subList(0, 4));
    Vector transformVector = new Vector2D(new double[] {doubles.get(4), doubles.get(5)});
    return new AffineTransform2D(transformMatrix, transformVector);
  }

  /**
   * Reads a Julia transformation from a line.
   *
   * @param line The line to read from.
   * @return A Julia transformation.
   */
  private static Transform readJuliaTransform(String line, int sign) {
    List<Double> doubles = parseDoubles(line);
    if (doubles.size() != 2) {
      throw new IllegalArgumentException("Julia transform must have 2 components");
    }
    Complex point = new Complex(doubles.get(0), doubles.get(1));
    return new JuliaTransform(point, sign);
  }

  /**
   * Reads a chaos game description from a file.
   *
   * @param filePath The name of the file to read from.
   * @return A chaos game description.
   * @throws Exception if the file is not of the correct format.
   */
  public static ChaosGameDescription readFromFile(String filePath) throws Exception {
    try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
      List<String> lines = getDataLines(reader);

      if (lines.size() < 4) {
        throw new IllegalArgumentException(
            "File must be of correct format, and contain at least 1 transform");
      }

      String transformType = lines.get(0);
      Vector lowerLeft = readVector2D(parseDoubles(lines.get(1)));
      Vector upperRight = readVector2D(parseDoubles(lines.get(2)));
      List<Transform> transforms = new ArrayList<>();

      if (transformType.equals("Affine2D")) {
        for (int i = 3; i < lines.size(); i++) {
          transforms.add(readAffineTransform2D(lines.get(i)));
        }
      } else if (transformType.equals("Julia")) {
        if (lines.size() != 4) {
          throw new IllegalArgumentException(
              "Julia transform must have a min and a max component, and a transform component.");
        }
        transforms.add(readJuliaTransform(lines.get(3), 1));
        transforms.add(readJuliaTransform(lines.get(3), -1));
      } else {
        throw new IllegalArgumentException("Unknown transform type: " + transformType);
      }
      return new ChaosGameDescription(transforms, lowerLeft, upperRight);
    }
  }

  /**
   * Gets the list of fractals defined in files.
   */
  public static List<String> getFractalNames() {
    List<String> fractals = new ArrayList<>();
    java.io.File folder = new java.io.File("src/main/resources/Fractals/");
    java.io.File[] listOfFiles = folder.listFiles();

    if (listOfFiles != null) {
      fractals.addAll(
          Arrays.stream(listOfFiles)
              .filter(java.io.File::isFile)
              .map(java.io.File::getName)
              .toList());
    }

    return fractals;
  }

  /**
   * Deletes a fractal defined in a file.
   *
   * @param fractalName The name of the fractal to delete.
   * @throws Exception if the file cannot be deleted.
   */
  public static void deleteFractal(String fractalName) throws Exception {
    if (!new java.io.File("src/main/resources/Fractals/" + fractalName).delete()) {
      throw new IOException("File could not be deleted: " + fractalName);
    }
  }

  /**
   * Writes a transform to a file.
   *
   * @param transform The transform to write.
   * @param writer    The writer to write to.
   */
  private static void writeTransform(Transform transform, BufferedWriter writer) {
    try {
      writer.write(affineTransformToString((AffineTransform2D) transform));
      writer.newLine();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Writes a chaos game description to a file.
   *
   * @param filePath             The name of the file to write to.
   * @param chaosGameDescription The chaos game description to write.
   * @throws java.io.IOException if the file cannot be written to.
   */
  public static void writeToFile(ChaosGameDescription chaosGameDescription, String filePath)
      throws java.io.IOException {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
      List<Transform> transforms = chaosGameDescription.getTransforms();
      Vector lowerLeft = chaosGameDescription.getMinCoords();
      Vector upperRight = chaosGameDescription.getMaxCoords();

      String transformType = transforms.get(0) instanceof AffineTransform2D ? "Affine2D" : "Julia";
      writer.write(transformType + "# Type of transform");
      writer.newLine();

      writer.write(vectorToString(lowerLeft) + "# Lower left");
      writer.newLine();
      writer.write(vectorToString(upperRight) + "# Upper right");
      writer.newLine();

      if (transforms.get(0) instanceof AffineTransform2D) {
        transforms.forEach(transform -> writeTransform(transform, writer));
      } else if (transforms.get(0) instanceof JuliaTransform) {
        writer.write(juliaTransformToString((JuliaTransform) transforms.get(0)));
      }
    }
  }

  /**
   * Deletes a transform file.
   *
   * @param filePath The name of the file to delete.
   * @throws Exception if the file cannot be deleted.
   */
  public static void deleteFile(String filePath) throws Exception {
    if (!new java.io.File(filePath).delete()) {
      throw new IOException("File could not be deleted: " + filePath);
    }
  }

  /**
   * Converts a vector to a string.
   *
   * @param vector The vector to convert.
   * @return The vector as a string.
   */
  private static String vectorToString(Vector vector) {
    return vector.getVector()[0] + ", " + vector.getVector()[1];
  }

  /**
   * Converts an affine transform to a string.
   *
   * @param transform The affine transform to convert.
   * @return The affine transform as a string.
   */
  private static String affineTransformToString(AffineTransform2D transform) {
    String matrixString = Arrays.stream(transform.getMatrix().getMatrix())
        .flatMapToDouble(Arrays::stream)
        .mapToObj(Double::toString)
        .collect(Collectors.joining(", "));

    String vectorString = Arrays.stream(transform.getVector().getVector())
        .mapToObj(Double::toString)
        .collect(Collectors.joining(", "));

    return matrixString + ", " + vectorString;
  }

  /**
   * Converts a Julia transform to a string.
   *
   * @param transform The Julia transform to convert.
   * @return The Julia transform as a string.
   */
  private static String juliaTransformToString(JuliaTransform transform) {
    return transform.getPoint().getReal() + ", " + transform.getPoint().getImaginary();
  }
}
