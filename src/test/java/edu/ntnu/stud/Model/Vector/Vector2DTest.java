package edu.ntnu.stud.Model.Vector;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * JUnit tests for the Vector2D class, covering both positive and negative scenarios.
 */
class Vector2DTest {
  private Vector vector1;
  private Vector vector2;

  /** Initializes two vectors available for all test methods in this class */
  @BeforeEach
  public void initTwoVector2D() {
    vector1 = new Vector2D(new double[]{1.0, 2.0});
    vector2 = new Vector2D(new double[]{-1.0, 3.0});
  }

  /** Positive test cases for Vector2D. */
  @Nested
  @DisplayName("Positive tests for Vector2D class.")
  class PositiveTests {

    @Test
    @DisplayName("Test valid vector creation.")
    void constructor_validVector_creationSuccessful() {
      double[] validVector = {1.0, 2.0};
      Vector2D vector = new Vector2D(validVector);
      assertArrayEquals(validVector, vector.getVector());
    }

    @Test
    @DisplayName("Test add method with valid vectors.")
    void add_validVectors_returnCorrectSum() {
      Vector expectedResult = new Vector2D(new double[]{0.0, 5.0});
      assertEquals(expectedResult, vector1.add(vector2));
    }

    @Test
    @DisplayName("Test subtract method with valid vectors.")
    void subtract_validVectors_returnCorrectDifference() {
      Vector expectedResult = new Vector2D(new double[]{2.0, -1.0});
      assertEquals(expectedResult, vector1.subtract(vector2));
    }

    @Test
    @DisplayName("Test equals method with same instance.")
    void equals_sameInstance_returnTrue() {
      assertEquals(vector1, vector1);
    }

    @Test
    @DisplayName("Test equals method with different instances, same values.")
    void equals_differentInstancesSameValues_returnTrue() {
      Vector vector1Copy = new Vector2D(new double[]{1.0, 2.0});
      assertEquals(vector1, vector1Copy);
    }
  }

  /** Negative test cases for Vector2D. */
  @Nested
  @DisplayName("Negative tests for Vector2D class.")
  class NegativeTests {

    @Test
    @DisplayName("Test constructor with invalid vector dimension.")
    void constructor_invalidVectorDimension_throwIllegalArgumentException() {
      double[] invalidVector = {1.0, 2.0, 3.0};
      assertThrows(IllegalArgumentException.class, () -> new Vector2D(invalidVector));
    }

    @Test
    @DisplayName("Test equals method with different instances, different values.")
    void equals_differentInstancesDifferentValues_returnFalse() {
      Vector differentVector = new Vector2D(new double[]{1.0, 3.0});
      assertNotEquals(vector1, differentVector);
    }
  }
}
