package edu.ntnu.stud.Model.ChaosGame;

import edu.ntnu.stud.Model.Transformation.Transform;
import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;
import java.util.Random;

/**
 * Class responsible for running a chaos game in separate thread.
 */
public class ChaosGame extends Thread {
  private final ChaosGameManager chaosGameManager;
  private Vector currentPoint;
  public Random random;
  private final int steps;

  /**
   * Creates a new chaos game.
   *
   * @throws IllegalArgumentException if the description is null.
   */
  public ChaosGame(ChaosGameManager chaosGameManager, int steps) {
    this.chaosGameManager = chaosGameManager;
    this.currentPoint = new Vector2D(new double[] {0, 0});
    this.random = new Random();
    this.steps = steps;
  }

  @Override
  public void run() {
    try {
      for (int i = 0; i < steps; i++) {
        int index = random.nextInt(chaosGameManager.getCountOfTransforms());
        Transform transform = chaosGameManager.getTransform(index);

        currentPoint = transform.transform(currentPoint);

        chaosGameManager.putPixel(currentPoint);
      }
      chaosGameManager.notifyObservers("ChaosGameComplete");
    } catch (Exception e) {
      Thread.currentThread().interrupt();
      throw e;
    }
  }
}
