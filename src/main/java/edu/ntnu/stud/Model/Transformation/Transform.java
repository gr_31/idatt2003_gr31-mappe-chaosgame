package edu.ntnu.stud.Model.Transformation;

import edu.ntnu.stud.Model.Vector.Vector;

/**
 * Interface for Transformation.
 */
public interface Transform {
  Vector transform(Vector point);
}
