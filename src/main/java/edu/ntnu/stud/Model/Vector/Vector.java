package edu.ntnu.stud.Model.Vector;

/**
 * Interface for Vector.
 */
public interface Vector {
  double[] getVector();

  Vector add(Vector otherVector);

  Vector subtract(Vector otherVector);
}