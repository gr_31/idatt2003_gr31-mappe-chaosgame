package edu.ntnu.stud.Model.ChaosGameDescription;

/**
 * Enum for the type of chaos game description.
 */
public enum ChaosGameDescriptionType {
  SIERPINSKI,
  JULIA
}

