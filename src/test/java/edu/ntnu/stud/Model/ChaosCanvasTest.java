package edu.ntnu.stud.Model;

import edu.ntnu.stud.Model.Matrix.Matrix;
import edu.ntnu.stud.Model.Matrix.Matrix2D;
import edu.ntnu.stud.Model.Transformation.AffineTransform2D;
import edu.ntnu.stud.Model.Transformation.Transform;
import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * JUnit tests for the ChaosCanvas class, covering both positive and negative scenarios.
 */
public class ChaosCanvasTest {

  private ChaosCanvas canvas;

  @BeforeEach
  void setUp() {
    Vector minCoords = new Vector2D(new double[] {0.0, 0.0});
    Vector maxCoords = new Vector2D(new double[] {10.0, 10.0});
    canvas = new ChaosCanvas(100, 100, minCoords, maxCoords);
  }

  /** Positive test cases for ChaosCanvas. */
  @Nested
  @DisplayName("Positive tests for ChaosCanvas class.")
  class PositiveTests {

    @Test
    @DisplayName("Test constructor with valid inputs.")
    void constructor_ValidInputs_CreatesInstance() {
      Vector minCoords = new Vector2D(new double[] {0.0, 0.0});
      Vector maxCoords = new Vector2D(new double[] {10.0, 10.0});
      ChaosCanvas canvas = new ChaosCanvas(100, 100, minCoords, maxCoords);
      assertNotNull(canvas);
    }

    @Test
    @DisplayName("Test getPixel with valid coordinates.")
    void getPixel_ValidCoords_ReturnsPixelValue() {
      Vector coords = new Vector2D(new double[] {5.0, 5.0});
      assertEquals(0, canvas.getPixel(coords));
    }

    @Test
    @DisplayName("Test putPixel with valid coordinates.")
    void putPixel_ValidCoords_IncrementsPixelValue() {
      Vector coords = new Vector2D(new double[] {5.0, 5.0});
      canvas.putPixel(coords);
      assertEquals(1, canvas.getPixel(coords));
    }

    @Test
    @DisplayName("Test getMaxPixelValue after putting a pixel.")
    void getMaxPixelValue_AfterPutPixel_ReturnsMaxPixelValue() {
      Vector coords = new Vector2D(new double[] {5.0, 5.0});
      canvas.putPixel(coords);
      assertEquals(1, canvas.getMaxPixelValue());
    }

    @Test
    @DisplayName("Test setMinCoords with valid input.")
    void setMinCoords_ValidInput_UpdatesMinCoords() {
      Vector newMinCoords = new Vector2D(new double[] {-10.0, -10.0});
      canvas.setMinCoords(newMinCoords);
      Vector testCoords = new Vector2D(new double[] {-10.0, -10.0});
      canvas.putPixel(testCoords);
      assertEquals(1, canvas.getPixel(testCoords));
    }

    @Test
    @DisplayName("Test setMaxCoords with valid input.")
    void setMaxCoords_ValidInput_UpdatesMaxCoords() {
      Vector newMaxCoords = new Vector2D(new double[] {20.0, 20.0});
      canvas.setMaxCoords(newMaxCoords);
      Vector testCoords = new Vector2D(new double[] {20.0, 20.0});
      canvas.putPixel(testCoords);
      assertEquals(1, canvas.getPixel(testCoords));
    }

    @Test
    @DisplayName("Test setCanvasSize with valid input.")
    void setCanvasSize_ValidInput_UpdatesCanvasSize() {
      canvas.setCanvasSize(200);
      assertEquals(200, canvas.getCanvasArray().length);
    }

    @Test
    @DisplayName("Test clear method.")
    void clear_ClearsCanvas() {
      Vector coords = new Vector2D(new double[] {5.0, 5.0});
      canvas.putPixel(coords);
      canvas.clear();
      assertEquals(0, canvas.getPixel(coords));
      assertEquals(0, canvas.getMaxPixelValue());
    }
  }

  /** Negative test cases for ChaosCanvas. */
  @Nested
  @DisplayName("Negative tests for ChaosCanvas class.")
  class NegativeTests {

    @Test
    @DisplayName("Test constructor with null minCoords.")
    void constructor_NullMinCoords_ThrowsIllegalArgumentException() {
      Vector maxCoords = new Vector2D(new double[] {10.0, 10.0});
      assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(100, 100, null, maxCoords));
    }

    @Test
    @DisplayName("Test constructor with null maxCoords.")
    void constructor_NullMaxCoords_ThrowsIllegalArgumentException() {
      Vector minCoords = new Vector2D(new double[] {0.0, 0.0});
      assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(100, 100, minCoords, null));
    }

    @Test
    @DisplayName("Test constructor with invalid width.")
    void constructor_InvalidWidth_ThrowsIllegalArgumentException() {
      Vector minCoords = new Vector2D(new double[] {0.0, 0.0});
      Vector maxCoords = new Vector2D(new double[] {10.0, 10.0});
      assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(-100, 100, minCoords, maxCoords));
    }

    @Test
    @DisplayName("Test constructor with invalid height.")
    void constructor_InvalidHeight_ThrowsIllegalArgumentException() {
      Vector minCoords = new Vector2D(new double[] {0.0, 0.0});
      Vector maxCoords = new Vector2D(new double[] {10.0, 10.0});
      assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(100, -100, minCoords, maxCoords));
    }

    @Test
    @DisplayName("Test putPixel with invalid coordinates type.")
    void putPixel_InvalidCoordsType_ThrowsIllegalArgumentException() {
      Vector invalidCoords = new Vector() {
        @Override
        public double[] getVector() {
          return new double[0];
        }

        @Override
        public Vector add(Vector vector) {
          return null;
        }

        @Override
        public Vector subtract(Vector vector) {
          return null;
        }
      };
      assertThrows(IllegalArgumentException.class, () -> canvas.putPixel(invalidCoords));
    }

    @Test
    @DisplayName("Test setMinCoords with null input.")
    void setMinCoords_NullInput_ThrowsIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> canvas.setMinCoords(null));
    }

    @Test
    @DisplayName("Test setMaxCoords with null input.")
    void setMaxCoords_NullInput_ThrowsIllegalArgumentException() {
      assertThrows(IllegalArgumentException.class, () -> canvas.setMaxCoords(null));
    }
  }
}
