package edu.ntnu.stud.Model.ChaosGameDescription;

import edu.ntnu.stud.Model.Transformation.Transform;
import edu.ntnu.stud.Model.Vector.Vector;
import java.util.List;

/**
 * Class for a chaos game description.
 */
public class ChaosGameDescription {
  private final Vector minCoords;
  private final Vector maxCoords;
  private final List<Transform> transforms;

  /**
   * Creates a new ChaosGameDescription.
   *
   * @param transforms The list of transforms to use.
   * @param minCoords  The minimum coordinates.
   * @param maxCoords  The maximum coordinates.
   */
  public ChaosGameDescription(List<Transform> transforms, Vector minCoords, Vector maxCoords) {
    if (transforms == null || minCoords == null || maxCoords == null) {
      throw new IllegalArgumentException("Arguments cannot be null");
    }

    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transforms = transforms;
  }

  /**
   * Gets the minimum coordinates.
   *
   * @return The minimum coordinates.
   */
  public Vector getMinCoords() {
    return minCoords;
  }

  /**
   * Gets the maximum coordinates.
   *
   * @return The maximum coordinates.
   */
  public Vector getMaxCoords() {
    return maxCoords;
  }

  /**
   * Gets the list of transforms.
   *
   * @return The list of transforms.
   */
  public List<Transform> getTransforms() {
    return transforms;
  }

  /**
   * Checks if two ChaosGameDescription objects are equal.
   *
   * @param obj The object to compare with.
   * @return True if the objects are equal, false otherwise.
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (!(obj instanceof ChaosGameDescription other)) {
      return false;
    }

    return minCoords.equals(other.minCoords)
        && maxCoords.equals(other.maxCoords)
        && transforms.equals(other.transforms);
  }
}
