package edu.ntnu.stud.Model.Matrix;

import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testing class for the Matrix2D class.
 */
class Matrix2DTest {

  /**
   * Nested class containing positive tests for the Matrix2D class.
   */
  @Nested
  @DisplayName("Positive tests for Matrix2D class.")
  class PositiveTests {
    Matrix matrix2D1;
    Matrix matrix2D2;

    @BeforeEach
    void init() {
      matrix2D1 = new Matrix2D(new double[][] {{1, 2}, {3, 4}});
      matrix2D2 = new Matrix2D(new double[][] {{1, 2}, {3, 4}});
    }

    @Test
    @DisplayName("Test constructor with 2x2 matrix.")
    void constructor_validMatrix2D_getterReturnsInput() {
      double[][] matrix = {{1, 2}, {3, 4}};
      assertEquals(matrix, new Matrix2D(matrix).getMatrix());
    }

    @Test
    @DisplayName("Test multiplyVector with 2x2 matrix.")
    void multiplyVector_vectorMatrix2D_multiplicationReturnsExpectedValue() {
      Vector vector2D = new Vector2D(new double[] {1, 2});
      assertEquals(new Vector2D(new double[] {5, 11}), matrix2D1.multiplyVector(vector2D));
    }

    @Test
    @DisplayName("Test equals with 2x2 matrix, where both matrix's is the same instance.")
    void equals_sameInstance_returnTrue() {
      assertEquals(matrix2D1, matrix2D1);
    }

    @Test
    @DisplayName("Test equals with 2x2 matrix, where both matrices contain the same fields.")
    void equals_differentInstancesSameValues_returnTrue() {
      assertEquals(matrix2D1, matrix2D2);
    }
  }

  /**
   * Nested class containing negative tests for the Matrix2D class.
   */
  @Nested
  @DisplayName("Negative tests for Matrix2D class.")
  class NegativeTests {

    @Test
    @DisplayName("Test constructor with 3x2 matrix.")
    void constructor_matrixNot2D_throwIllegalArgumentException() {
      double[][] matrix = {{1, 2, 3}, {4, 5, 6}};
      assertThrows(IllegalArgumentException.class, () -> new Matrix2D(matrix));
    }

    @Test
    @DisplayName("Test equals with 2x2 matrix, where both matrices contain different fields.")
    void equals_differentInstancesDifferentValues_returnFalse() {
      double[][] matrix1 = {{1, 2}, {3, 4}};
      double[][] matrix2 = {{5, 6}, {7, 8}};
      Matrix matrix2D1 = new Matrix2D(matrix1);
      Matrix matrix2D2 = new Matrix2D(matrix2);
      assertNotEquals(matrix2D1, matrix2D2);
    }
  }
}
