package edu.ntnu.stud.Model.ChaosGameDescription;

import edu.ntnu.stud.Model.Matrix.Matrix2D;
import edu.ntnu.stud.Model.Transformation.AffineTransform2D;
import edu.ntnu.stud.Model.Transformation.JuliaTransform;
import edu.ntnu.stud.Model.Vector.Complex;
import edu.ntnu.stud.Model.Vector.Vector2D;
import java.util.List;

/**
 * Factory class for creating ChaosGameDescription objects.
 */
public class ChaosGameDescriptionFactory {
  /**
   * Creates a ChaosGameDescription object based on the given type.
   *
   * @param type The type of chaos game description to create.
   * @return A ChaosGameDescription object.
   */
  public static ChaosGameDescription buildChaosGameDescription(ChaosGameDescriptionType type) {
    switch (type) {
      case SIERPINSKI -> {
        Matrix2D transformMatrix1 = new Matrix2D(new double[][] {{0.5, 0.0}, {0.0, 0.5}});
        Matrix2D transformMatrix2 = new Matrix2D(new double[][] {{0.5, 0.0}, {0.0, 0.5}});
        Matrix2D transformMatrix3 = new Matrix2D(new double[][] {{0.5, 0.0}, {0.0, 0.5}});
        Vector2D transformVector1 = new Vector2D(new double[] {0.0, 0.0});
        Vector2D transformVector2 = new Vector2D(new double[] {0.25, 0.5});
        Vector2D transformVector3 = new Vector2D(new double[] {0.5, 0.0});
        AffineTransform2D transform1 = new AffineTransform2D(transformMatrix1, transformVector1);
        AffineTransform2D transform2 = new AffineTransform2D(transformMatrix2, transformVector2);
        AffineTransform2D transform3 = new AffineTransform2D(transformMatrix3, transformVector3);
        return new ChaosGameDescription(List.of(transform1, transform2, transform3),
            new Vector2D(new double[] {0.0, 0.0}), new Vector2D(new double[] {1.0, 1.0}));
      }
      case JULIA -> {
        Complex point = new Complex(-0.74543, 0.11301);
        JuliaTransform juliaTransform1 = new JuliaTransform(point, 1);
        JuliaTransform juliaTransform2 = new JuliaTransform(point, -1);
        return new ChaosGameDescription(List.of(juliaTransform1, juliaTransform2),
            new Vector2D(new double[] {-1.6, -1.0}), new Vector2D(new double[] {1.6, 1.0}));
      }
      default -> throw new IllegalArgumentException("Invalid ChaosGameDescriptionType");
    }
  }
}
