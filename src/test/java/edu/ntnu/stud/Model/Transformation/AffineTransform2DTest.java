package edu.ntnu.stud.Model.Transformation;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.stud.Model.Matrix.Matrix;
import edu.ntnu.stud.Model.Matrix.Matrix2D;
import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/** Test class for AffineTransform2D. */
class AffineTransform2DTest {

  /** Positive test cases for AffineTransform2D. */
  @Nested
  @DisplayName("Positive tests for AffineTransform2D class.")
  class PositiveTests {

    @Test
    @DisplayName("Test transform with identity matrix and vector addition.")
    void transform_identityMatrixAndVector_additionCorrect() {
      Matrix matrix = new Matrix2D(new double[][] {{1, 0}, {0, 1}});
      Vector vector = new Vector2D(new double[] {1, 2});

      Transform affineTransform2D = new AffineTransform2D(matrix, vector);
      Vector inputVector = new Vector2D(new double[] {3, 4});
      Vector expectedResult = new Vector2D(new double[] {4, 6});

      Vector actualResult = affineTransform2D.transform(inputVector);

      assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("Test equals method with same instance.")
    void equals_sameInstance_returnTrue() {
      Matrix matrix = new Matrix2D(new double[][] {{1, 0}, {0, 1}});
      Vector vector = new Vector2D(new double[] {1, 2});
      AffineTransform2D affineTransform2D = new AffineTransform2D(matrix, vector);
      assertEquals(affineTransform2D, affineTransform2D);
    }

    @Test
    @DisplayName("Test equals method with different instances, same values.")
    void equals_differentInstancesSameValues_returnTrue() {
      Matrix matrix = new Matrix2D(new double[][] {{1, 0}, {0, 1}});
      Vector vector = new Vector2D(new double[] {1, 2});
      AffineTransform2D affineTransform2D1 = new AffineTransform2D(matrix, vector);
      AffineTransform2D affineTransform2D2 = new AffineTransform2D(matrix, vector);
      assertEquals(affineTransform2D1, affineTransform2D2);
    }
  }

  /** Negative test cases for AffineTransform2D. */
  @Nested
  @DisplayName("Negative tests for AffineTransform2D class.")
  class NegativeTests {

    @Test
    @DisplayName("Test equals method with different instances, different values.")
    void equals_differentInstancesDifferentValues_returnFalse() {
      Matrix matrix1 = new Matrix2D(new double[][] {{1, 0}, {0, 1}});
      Vector vector1 = new Vector2D(new double[] {1, 2});
      AffineTransform2D affineTransform2D1 = new AffineTransform2D(matrix1, vector1);

      Matrix matrix2 = new Matrix2D(new double[][] {{1, 0}, {0, 2}});
      Vector vector2 = new Vector2D(new double[] {1, 3});
      AffineTransform2D affineTransform2D2 = new AffineTransform2D(matrix2, vector2);

      assertNotEquals(affineTransform2D1, affineTransform2D2);
    }

    @Test
    @DisplayName("Test constructor with null matrix.")
    void constructor_nullMatrix_throwIllegalArgumentException() {
      Vector vector = new Vector2D(new double[] {1, 2});
      assertThrows(IllegalArgumentException.class, () -> new AffineTransform2D(null, vector));
    }

    @Test
    @DisplayName("Test constructor with null vector.")
    void constructor_nullVector_throwIllegalArgumentException() {
      Matrix matrix = new Matrix2D(new double[][] {{1, 0}, {0, 1}});
      assertThrows(IllegalArgumentException.class, () -> new AffineTransform2D(matrix, null));
    }
  }
}
