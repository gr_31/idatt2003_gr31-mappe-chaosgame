package edu.ntnu.stud.Model.Vector;

/**
 * Class representing a 2D vector.
 */
public class Vector2D implements Vector {
  private final double[] vector;

  /**
   * Creates a 2D vector.
   *
   * @throws IllegalArgumentException if the vector is not 2D
   */
  public Vector2D(double[] vector) {
    if (vector.length != 2) {
      throw new IllegalArgumentException("Vector must be 2x2");
    }
    this.vector = vector;
  }

  @Override
  public double[] getVector() {
    return this.vector;
  }

  /**
   * Creates a new 2D vector by adding this vector to another vector.
   *
   * @param otherVector The vector to be added to this vector.
   * @return result A new vector representing the result of addition.
   * @thorws IllegalArgumentException if the vector is not 2D
   */
  @Override
  public Vector add(Vector otherVector) {
    if (otherVector.getVector().length != 2) {
      throw new IllegalArgumentException("Vector must be 2D");
    }
    double resultX1 = this.getVector()[0] + otherVector.getVector()[0];
    double resultX2 = this.getVector()[1] + otherVector.getVector()[1];

    double[] resultVector = {resultX1, resultX2};

    return new Vector2D(resultVector);
  }

  /**
   * Creates a new 2D vector by subtracting another vector from this vector.
   *
   * @param otherVector The vector to be subtracted from this vector.
   * @return The resulting vector.
   * @throws IllegalArgumentException if the vector is not 2D
   */
  @Override
  public Vector subtract(Vector otherVector) {
    if (otherVector.getVector().length != 2) {
      throw new IllegalArgumentException("Vector must be 2D");
    }

    double resultX1 = this.getVector()[0] - otherVector.getVector()[0];
    double resultX2 = this.getVector()[1] - otherVector.getVector()[1];

    double[] resultVector = {resultX1, resultX2};

    return new Vector2D(resultVector);
  }

  /**
   * Checks if two Vector2D objects are equal.
   *
   * @param obj The object to compare with.
   * @return True if the objects are equal, false otherwise.
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (!(obj instanceof Vector2D other)) {
      return false;
    }

    return this.getVector()[0] == other.getVector()[0]
        && this.getVector()[1] == other.getVector()[1];
  }
}
