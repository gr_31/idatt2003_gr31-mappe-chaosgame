package edu.ntnu.stud.Model.Transformation;

import edu.ntnu.stud.Model.Vector.Complex;
import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;

/**
 * Class for Julia transformation.
 *
 * <p>The field point represents the complex number c in the transformation +-sqrt(z-c) The field
 * sign represents positive and negative value
 *
 * @author 10040
 */
public class JuliaTransform implements Transform {
  private final Complex point;
  private final int sign;

  /**
   * Creates a JuliaTransform .
   *
   * @throws IllegalArgumentException if the sign is neither -1 nor +1
   * @author 10040
   */
  public JuliaTransform(Complex point, int sign) {
    if (sign != 1 && sign != -1) {
      throw new IllegalArgumentException("Sign must be +1 or -1");
    }
    if(point == null) {
      throw new IllegalArgumentException("Point cannot be null");
    }

    this.point = point;
    this.sign = sign;
  }

  /**
   * Gets the point.
   *
   * @return The point.
   */
  public Complex getPoint() {
    return point;
  }

  /**
   * Transforms a complex number represented as a Vector2D to the result of +- sqrt(z-c)
   *
   * @param vectorZ The complex number which will be transformed, imported as Vector2D.
   * @return The result of the transformation as Vector2D.
   */
  public Vector transform(Vector vectorZ) {
    Vector zPartSubtract = vectorZ.subtract(point);
    Complex zComplex = new Complex(zPartSubtract.getVector()[0], zPartSubtract.getVector()[1]);
    zComplex = zComplex.sqrt();
    double zRealCompleted = zComplex.getReal() * sign;
    double zImaginaryCompleted = zComplex.getImaginary() * sign;
    return new Vector2D(new double[] {zRealCompleted, zImaginaryCompleted});
  }


  /**
   * Checks if two JuliaTransform objects are equal.
   *
   * @param obj The object to compare with.
   * @return True if the objects are equal, false otherwise.
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (!(obj instanceof JuliaTransform otherTransform)) {
      return false;
    }

    return point.equals(otherTransform.point) && sign == otherTransform.sign;
  }
}
