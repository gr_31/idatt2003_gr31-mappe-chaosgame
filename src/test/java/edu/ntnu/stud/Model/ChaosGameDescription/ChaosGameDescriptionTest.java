package edu.ntnu.stud.Model.ChaosGameDescription;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.stud.Model.Transformation.JuliaTransform;
import edu.ntnu.stud.Model.Transformation.Transform;
import edu.ntnu.stud.Model.Vector.Complex;
import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Test class for ChaosGameDescription.
 */
class ChaosGameDescriptionTest {

  /**
   * Positive test cases for ChaosGameDescription.
   */
  @Nested
  @DisplayName("Positive tests for ChaosGameDescription.")
  class PositiveTests {

    @Test
    @DisplayName("Test constructor with 2D Julia Transforms.")
    void constructor_validParams_gettersReturnInput() {
      Vector minCoords = new Vector2D(new double[] {1, 2});
      Vector maxCoords = new Vector2D(new double[] {3, 4});

      Complex complex = new Complex(1, 2);

      List<Transform> transforms = new ArrayList<>();
      transforms.add(new JuliaTransform(complex, 1));

      ChaosGameDescription chaosGameDescription =
          new ChaosGameDescription(transforms, minCoords, maxCoords);

      assertEquals(minCoords.getVector(), chaosGameDescription.getMinCoords().getVector());
      assertEquals(maxCoords.getVector(), chaosGameDescription.getMaxCoords().getVector());
      assertEquals(transforms, chaosGameDescription.getTransforms());
    }

    @Test
    @DisplayName("Test equals method with same instance.")
    void equals_sameInstance_returnTrue() {
      Vector minCoords = new Vector2D(new double[] {1, 2});
      Vector maxCoords = new Vector2D(new double[] {3, 4});

      Complex complex = new Complex(1, 2);

      List<Transform> transforms = new ArrayList<>();
      transforms.add(new JuliaTransform(complex, 1));

      ChaosGameDescription chaosGameDescription =
          new ChaosGameDescription(transforms, minCoords, maxCoords);

      assertEquals(chaosGameDescription, chaosGameDescription);
    }

    @Test
    @DisplayName("Test equals method with different instances, same values.")
    void equals_differentInstancesSameValues_returnTrue() {
      Vector minCoords = new Vector2D(new double[] {1, 2});
      Vector maxCoords = new Vector2D(new double[] {3, 4});

      Complex complex = new Complex(1, 2);

      List<Transform> transforms = new ArrayList<>();
      transforms.add(new JuliaTransform(complex, 1));

      ChaosGameDescription chaosGameDescription1 =
          new ChaosGameDescription(transforms, minCoords, maxCoords);
      ChaosGameDescription chaosGameDescription2 =
          new ChaosGameDescription(transforms, minCoords, maxCoords);

      assertEquals(chaosGameDescription1, chaosGameDescription2);
    }
  }

  /**
   * Negative test cases for ChaosGameDescription.
   */
  @Nested
  @DisplayName("Negative tests for ChaosGameDescription")
  class NegativeTests {
    @Test
    @DisplayName("Test equals method with different instances, different values.")
    void equals_differentInstancesDifferentValues_returnFalse() {
      Vector minCoords1 = new Vector2D(new double[] {1, 2});
      Vector maxCoords1 = new Vector2D(new double[] {3, 4});

      Complex complex1 = new Complex(1, 2);

      List<Transform> transforms1 = new ArrayList<>();
      transforms1.add(new JuliaTransform(complex1, 1));

      ChaosGameDescription chaosGameDescription1 =
          new ChaosGameDescription(transforms1, minCoords1, maxCoords1);

      Vector minCoords2 = new Vector2D(new double[] {2, 2});
      Vector maxCoords2 = new Vector2D(new double[] {2, 4});

      Complex complex2 = new Complex(1, 3);

      List<Transform> transforms2 = new ArrayList<>();
      transforms2.add(new JuliaTransform(complex2, 1));

      ChaosGameDescription chaosGameDescription2 =
          new ChaosGameDescription(transforms2, minCoords2, maxCoords2);

      assertNotEquals(chaosGameDescription1, chaosGameDescription2);
    }

    @Test
    @DisplayName("Test constructor with null transforms.")
    void constructor_nullTransforms_throwIllegalArgumentException() {
      Vector minCoords = new Vector2D(new double[] {1, 2});
      Vector maxCoords = new Vector2D(new double[] {3, 4});

      Exception exception = assertThrows(IllegalArgumentException.class, () -> {
        new ChaosGameDescription(null, minCoords, maxCoords);
      });

      assertEquals("Arguments cannot be null", exception.getMessage());
    }

    @Test
    @DisplayName("Test constructor with null minCoords.")
    void constructor_nullMinCoords_throwIllegalArgumentException() {
      Vector maxCoords = new Vector2D(new double[] {3, 4});
      List<Transform> transforms = new ArrayList<>();

      Exception exception = assertThrows(IllegalArgumentException.class, () -> {
        new ChaosGameDescription(transforms, null, maxCoords);
      });

      assertEquals("Arguments cannot be null", exception.getMessage());
    }

    @Test
    @DisplayName("Test constructor with null maxCoords.")
    void constructor_nullMaxCoords_throwIllegalArgumentException() {
      Vector minCoords = new Vector2D(new double[] {1, 2});
      List<Transform> transforms = new ArrayList<>();

      Exception exception = assertThrows(IllegalArgumentException.class, () -> {
        new ChaosGameDescription(transforms, minCoords, null);
      });

      assertEquals("Arguments cannot be null", exception.getMessage());
    }
  }
}