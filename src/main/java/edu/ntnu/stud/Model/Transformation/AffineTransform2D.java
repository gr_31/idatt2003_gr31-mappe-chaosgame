package edu.ntnu.stud.Model.Transformation;

import edu.ntnu.stud.Model.Matrix.Matrix;
import edu.ntnu.stud.Model.Vector.Vector;

/**
 * Class for 2D affine transformation.
 *
 * <p>The field matrix represents the matrix in the transformation The field vector represents the
 * vector in the transformation
 */
public class AffineTransform2D implements Transform {
  private final Matrix matrix;
  private final Vector vector;

  /**
   * Creates a new 2D affine transformation.
   *
   * @param matrix The matrix to use.
   * @param vector The vector to use.
   */
  public AffineTransform2D(Matrix matrix, Vector vector) {
    if (matrix == null || vector == null) {
      throw new IllegalArgumentException("Arguments cannot be null");
    }

    this.matrix = matrix;
    this.vector = vector;
  }

  /**
   * Gets the matrix.
   *
   * @return The matrix.
   */
  public Matrix getMatrix() {
    return matrix;
  }

  /**
   * Gets the vector.
   *
   * @return The vector.
   */
  public Vector getVector() {
    return vector;
  }

  /**
   * Transforms a vector.
   *
   * @param vector The vector to transform.
   * @return The transformed vector.
   */
  public Vector transform(Vector vector) {
    return matrix.multiplyVector(vector).add(this.vector);
  }

  /**
   * Checks if two AffineTransform2D objects are equal.
   *
   * @param obj The object to compare with.
   * @return True if the objects are equal, false otherwise.
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (!(obj instanceof AffineTransform2D other)) {
      return false;
    }

    return matrix.equals(other.getMatrix()) && vector.equals(other.getVector());
  }
}
