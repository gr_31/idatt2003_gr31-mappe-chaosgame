package edu.ntnu.stud.Model.ChaosGame;

import edu.ntnu.stud.Controller.ExceptionHandler;
import edu.ntnu.stud.Controller.Observer;
import edu.ntnu.stud.Model.ChaosCanvas;
import edu.ntnu.stud.Model.ChaosGameDescription.ChaosGameDescription;
import edu.ntnu.stud.Model.Transformation.Transform;
import edu.ntnu.stud.Model.Vector.Vector;
import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible for managing the chaos game.
 */
public class ChaosGameManager {
  private final ChaosCanvas canvas;
  private ChaosGameDescription description;
  private ChaosGame currentGame;
  List<Observer> observers;

  /**
   * Creates a new chaos game manager.
   *
   * @param description The description of the chaos game containing transformations.
   * @param width       The max width values from transformations.
   * @param height      The max height values from transformations.
   * @throws IllegalArgumentException if the description is null.
   */
  public ChaosGameManager(ChaosGameDescription description, int width, int height) {
    if (description == null) {
      throw new IllegalArgumentException("Description cannot be null");
    }

    this.description = description;
    this.canvas =
        new ChaosCanvas(width, height, description.getMinCoords(), description.getMaxCoords());
    this.observers = new ArrayList<>();
  }

  /**
   * Returns the canvas.
   *
   * @return The canvas.
   */
  public ChaosCanvas getCanvas() {
    return canvas;
  }

  /**
   * Returns the count of transforms.
   *
   * @return The count of transforms.
   */
  public int getCountOfTransforms() {
    return description.getTransforms().size();
  }

  /**
   * Returns the transform at the given index.
   *
   * @param index The index of the transform.
   * @return The transform at the given index.
   */
  public Transform getTransform(int index) {
    return description.getTransforms().get(index);
  }

  /**
   * Puts a pixel on the canvas.
   *
   * @param point The point to put on the canvas.
   */
  public void putPixel(Vector point) {
    canvas.putPixel(point);
  }

  /**
   * Interrupt the current chaos game thread.
   */
  private void interruptCurrentGame() {
    if (this.currentGame != null && this.currentGame.isAlive()) {
      try {
        this.currentGame.interrupt();
        notifyObservers("ChaosGameComplete");
      } catch (Exception e) {
        ExceptionHandler.getInstance()
            .logException(e, "An error occurred while interrupting the current game.");
        throw new RuntimeException("An error occurred while interrupting the current game.", e);
      }
    }
  }


  /**
   * Run the chaos game.
   *
   * @param steps The number of steps to run the chaos game.
   */
  public synchronized void runChaosGame(int steps) {
    interruptCurrentGame();

    this.canvas.clear();
    this.currentGame = new ChaosGame(this, steps);
    this.currentGame.start();
    notifyObservers("runningChaosGame");
  }

  /**
   * Add an observer to the chaos game.
   *
   * @param observer The observer to add.
   */
  public void addObserver(Observer observer) {
    observers.add(observer);
  }

  /**
   * Remove an observer from the chaos game.
   *
   * @param observer The observer to remove.
   */
  public void removeObserver(Observer observer) {
    observers.remove(observer);
  }

  /**
   * Notify all observers.
   *
   * @param message The message to send to the observers.
   */
  void notifyObservers(String message) {
    observers.forEach(observer -> observer.update(message, null));
  }

  /**
   * Sets the chaos game description.
   *
   * @param description The chaos game description.
   */
  public void setChaosGameDescription(ChaosGameDescription description) {
    if (description == null) {
      throw new IllegalArgumentException("Description cannot be null");
    }

    this.description = description;
    this.canvas.setMaxCoords(description.getMaxCoords());
    this.canvas.setMinCoords(description.getMinCoords());
  }

  /**
   * Sets the canvas size.
   *
   * @param size The size of the canvas.
   */
  public void setCanvasSize(int size) {
    interruptCurrentGame();
    this.canvas.setCanvasSize(size);
  }
}
