package edu.ntnu.stud.Model.Matrix;

import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;

/**
 * A 2x2 matrix object.
 */
public class Matrix2D implements Matrix {
  private final double[][] matrix;

  /**
   * Creates a new 2x2 matrix.
   *
   * @param matrix The matrix to use.
   * @throws IllegalArgumentException If the matrix is not 2x2.
   */
  public Matrix2D(double[][] matrix) {
    if (matrix.length != 2 || matrix[0].length != 2 || matrix[1].length != 2) {
      throw new IllegalArgumentException("Matrix must be 2x2");
    }
    this.matrix = matrix;
  }

  /**
   * Gets the matrix.
   *
   * @return The matrix.
   */
  @Override
  public double[][] getMatrix() {
    return matrix;
  }

  /**
   * Multiplies the matrix with a vector.
   *
   * @param vector The vector to multiply with.
   * @return The resulting vector.
   * @throws IllegalArgumentException If the vector is not of length 2.
   */
  @Override
  public Vector multiplyVector(Vector vector) {
    if (vector.getVector().length != 2) {
      throw new IllegalArgumentException("Vector must be of length 2");
    }

    double[] result = new double[2];

    for (int i = 0; i < 2; i++) {
      result[i] = matrix[i][0] * vector.getVector()[0] + matrix[i][1] * vector.getVector()[1];
    }

    return new Vector2D(result);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (!(obj instanceof Matrix2D other)) {
      return false;
    }

    return matrix[0][0] == other.matrix[0][0]
        && matrix[0][1] == other.matrix[0][1]
        && matrix[1][0] == other.matrix[1][0]
        && matrix[1][1] == other.matrix[1][1];
  }
}
