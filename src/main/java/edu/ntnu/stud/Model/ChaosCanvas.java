package edu.ntnu.stud.Model;

import edu.ntnu.stud.Controller.ExceptionHandler;
import edu.ntnu.stud.Model.Matrix.Matrix;
import edu.ntnu.stud.Model.Matrix.Matrix2D;
import edu.ntnu.stud.Model.Transformation.AffineTransform2D;
import edu.ntnu.stud.Model.Transformation.Transform;
import edu.ntnu.stud.Model.Vector.Vector;
import edu.ntnu.stud.Model.Vector.Vector2D;

/**
 * Class for the canvas It provides methods to manipulate pixels on the canvas and transform
 * coordinates to pixel indices.
 */
public class ChaosCanvas {
  private int[][] canvas;
  private int width;
  private int height;
  private Vector minCoords;
  private Vector maxCoords;
  private Transform transformCoordsToIndices;
  private int maxPixelValue = 0;

  /**
   * Constructs a ChaosCanvas object with the specified width, height, minimum coordinates, and
   * maximum coordinates.
   *
   * @param width     The width of the canvas.
   * @param height    The height of the canvas.
   * @param minCoords The minimum coordinates of the canvas.
   * @param maxCoords The maximum coordinates of the canvas.
   * @throws IllegalArgumentException if minCoords or maxCoords are not instances of Vector2D.
   */
  public ChaosCanvas(int width, int height, Vector minCoords, Vector maxCoords) {
    if (!(minCoords instanceof Vector2D) || !(maxCoords instanceof Vector2D)) {
      throw new IllegalArgumentException("minCoords and maxCoords must be of type Vector2D");
    }
    if (width <= 0 || height <= 0) {
      throw new IllegalArgumentException("Width and height must be positive");
    }

    this.width = width;
    this.height = height;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.canvas = new int[width][height];

    updateTransformCoordsToIndices();
  }

  /**
   * Updates the affine transformation for transforming coordinates to pixel.
   */
  private void updateTransformCoordsToIndices() {
    Matrix affineMatrix =
        new Matrix2D(
            new double[][] {
                {0, ((height - 1)
                    / (minCoords.getVector()[1] - maxCoords.getVector()[1]))},
                {((width - 1) / (maxCoords.getVector()[0] - minCoords.getVector()[0])), 0}
            });

    Vector affineVector =
        new Vector2D(
            new double[] {
                ((height - 1) * maxCoords.getVector()[1])
                    / (maxCoords.getVector()[1] - minCoords.getVector()[1]),
                ((width - 1) * minCoords.getVector()[0])
                    / (minCoords.getVector()[0] - maxCoords.getVector()[0])
            });

    transformCoordsToIndices = new AffineTransform2D(affineMatrix, affineVector);
  }

  /**
   * Gets the pixel value at the specified coordinates.
   *
   * @param coords The coordinates of the pixel.
   * @return The pixel value at the specified coordinates.
   * @throws IllegalArgumentException if coords is not an instance of Vector2D.
   */
  public synchronized int getPixel(Vector coords) {
    if (!(coords instanceof Vector2D)) {
      throw new IllegalArgumentException("coords must be of type Vector2D");
    }
    Vector pixelCoords = transformCoordsToIndices.transform(coords);
    return canvas[(int) pixelCoords.getVector()[0]][(int) pixelCoords.getVector()[1]];
  }

  /**
   * Gets the maximum pixel value.
   *
   * @return The maximum pixel value.
   */
  public synchronized int getMaxPixelValue() {
    return maxPixelValue;
  }

  /**
   * Sets the pixel value at the specified coordinates.
   *
   * @param coords The coordinates of the pixel.
   * @throws IllegalArgumentException if coords is not an instance of Vector2D.
   */
  public synchronized void putPixel(Vector coords) {
    if (!(coords instanceof Vector2D)) {

      throw new IllegalArgumentException("coords must be of type Vector2D");
    }
    Vector pixelCoords = transformCoordsToIndices.transform(coords);

    if (pixelCoords.getVector()[0] < 0
        || pixelCoords.getVector()[0] >= width
        || pixelCoords.getVector()[1] < 0
        || pixelCoords.getVector()[1] >= height) {
      return;
    }

    canvas[(int) pixelCoords.getVector()[0]][(int) pixelCoords.getVector()[1]]++;

    if (canvas[(int) pixelCoords.getVector()[0]][(int) pixelCoords.getVector()[1]]
        > maxPixelValue) {
      maxPixelValue = canvas[(int) pixelCoords.getVector()[0]][(int) pixelCoords.getVector()[1]];
    }
  }

  /**
   * Gets the canvas array representing the pixel values.
   *
   * @return The canvas array.
   */
  public synchronized int[][] getCanvasArray() {
    return canvas;
  }

  /**
   * Modifies the min coordinates for the chaos game, and update value to pixel transform.
   *
   * @param minCoords The new minimum coordinates from chaos game.
   */
  public synchronized void setMinCoords(Vector minCoords) {
    if (minCoords == null) {
      ExceptionHandler.getInstance()
          .logException(new IllegalArgumentException("minCoords cannot be null"),
              "ChaosCanvas.setMinCoords");
      throw new IllegalArgumentException("minCoords cannot be null");
    }

    clear();
    this.minCoords = minCoords;
    updateTransformCoordsToIndices();
  }

  /**
   * Modifies the max coordinates for the chaos game, and update value to pixel transform.
   *
   * @param maxCoords The new maximum coordinates from chaos game.
   */
  public synchronized void setMaxCoords(Vector maxCoords) {
    if (maxCoords == null) {
      ExceptionHandler.getInstance()
          .logException(new IllegalArgumentException("maxCoords cannot be null"),
              "ChaosCanvas.setMaxCoords");
      throw new IllegalArgumentException("maxCoords cannot be null");
    }

    clear();
    this.maxCoords = maxCoords;
    updateTransformCoordsToIndices();
  }

  /**
   * Modifies the width of the canvas, and update value to pixel transform.
   *
   * @param canvasSize The new width of the canvas.
   */
  public synchronized void setCanvasSize(int canvasSize) {
    this.width = canvasSize;
    this.height = canvasSize;
    clear();
    updateTransformCoordsToIndices();
  }

  /**
   * Clears the canvas by initializing the canvas array with zeros.
   */
  public synchronized void clear() {
    this.maxPixelValue = 0;
    this.canvas = new int[width][height];
  }
}
