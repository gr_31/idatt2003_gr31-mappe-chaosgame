package edu.ntnu.stud.Controller;

import edu.ntnu.stud.Model.ChaosGame.ChaosGameManager;
import edu.ntnu.stud.Model.ChaosGameDescription.ChaosGameDescription;
import edu.ntnu.stud.Model.ChaosGameDescription.ChaosGameDescriptionFactory;
import edu.ntnu.stud.Model.ChaosGameDescription.ChaosGameDescriptionType;
import edu.ntnu.stud.Model.FileHandling;
import edu.ntnu.stud.View.GUIComponents.ActionFeedback;
import edu.ntnu.stud.View.GUIComponents.NavBar;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for handling the fractal.
 */
public class FractalHandler implements Observer {
  private int countOfSteps = 100000;
  private ChaosGameManager currenChaosGameManager;
  private ChaosGameDescription currentChaosGameDescription;
  private final NavBar navBar;
  private int fractalSize;
  private final List<Observer> observers;

  /**
   * Constructor for FractalHandler.
   *
   * <p>Runs chaos game with Sierpinski triangle as default.
   */
  public FractalHandler(int fractalSize) {
    this.navBar = NavBar.getInstance();
    this.navBar.addObserver(this);
    this.navBar.addFractal(FileHandling.getFractalNames());
    this.navBar.addFractalTypes(ChaosGameDescriptionType.values());
    this.navBar.setIterations(String.valueOf(this.countOfSteps));

    this.fractalSize = fractalSize;

    this.observers = new ArrayList<>();

    this.currentChaosGameDescription =
        ChaosGameDescriptionFactory.buildChaosGameDescription(ChaosGameDescriptionType.SIERPINSKI);

    constructFractal();
  }

  /**
   * Resize the fractal.
   *
   * @param size the height and width of the fractal
   */
  public void fractalResize(int size) {
    this.fractalSize = size;
    constructFractal();
  }

  /**
   * Construct fractal from currentChaosGameDescription (private variable).
   */
  private void constructFractal() {
    try {
      if (this.currenChaosGameManager == null) {
        this.currenChaosGameManager =
            new ChaosGameManager(
                this.currentChaosGameDescription, this.fractalSize, this.fractalSize);
      } else {
        this.currenChaosGameManager.setChaosGameDescription(this.currentChaosGameDescription);
        this.currenChaosGameManager.setCanvasSize(this.fractalSize);
      }

      this.currenChaosGameManager.addObserver(this);

      runChaosGame();
    } catch (Exception e) {
      ActionFeedback.showErrorDialogBox(e.getMessage());
      ExceptionHandler.getInstance().logException(e, "Error in constructing fractal");
    }
  }

  /**
   * Run the chaos game.
   */
  private void runChaosGame() {
    currenChaosGameManager.runChaosGame(this.countOfSteps);
  }

  /**
   * Load a ChaosGame from a file.
   *
   * @param filePath the path to the file
   * @throws Exception if an error occurs
   */
  private void loadChaosGameFromFile(String filePath) throws Exception {
    this.currentChaosGameDescription = FileHandling.readFromFile(filePath);
  }

  /**
   * Add an observer.
   *
   * @param observer the observer to add
   */
  public void addObserver(Observer observer) {
    this.observers.add(observer);
  }

  /**
   * Remove an observer.
   *
   * @param observer the observer to remove
   */
  public void removeObserver(Observer observer) {
    this.observers.remove(observer);
  }

  /**
   * Notify observers of change.
   *
   * @param changedValueName the name of the changed value
   * @param newValue         the new value
   */
  private void notifyObservers(String changedValueName, Object newValue) {
    observers.forEach(observer -> observer.update(changedValueName, newValue));
  }

  /**
   * Update based on a change in other objects that are observed.
   *
   * <p>Updates the fractal based on the change: change of fractal, change of iterations. And delete
   * or fetch stored fractals.
   *
   * @param changedValueName the name of the changed value
   * @param newValue         the new value
   */
  public void update(String changedValueName, Object newValue) {
    try {
      switch (changedValueName) {
        case "Fractal" -> {
          loadChaosGameFromFile("src/main/resources/Fractals/" + newValue);
          constructFractal();
        }
        case "PredefinedFractal" -> {
          this.currentChaosGameDescription =
              ChaosGameDescriptionFactory.buildChaosGameDescription(
                  ChaosGameDescriptionType.valueOf((String) newValue));
          constructFractal();
        }
        case "Iterations" -> {
          try {
            if (((String) newValue).isEmpty()) {
              throw new IllegalArgumentException("Iterations cannot be empty");
            }

            if (!((String) newValue).matches("\\d+")) {
              throw new IllegalArgumentException("Iterations must be a valid positive number");
            }

            if (new java.math.BigInteger((String) newValue).compareTo(
                java.math.BigInteger.valueOf(2000000000))
                > 0) {
              throw new IllegalArgumentException("Iterations cannot be greater than 2000000000");
            }

            int newIterations = Integer.parseInt((String) newValue);
            if (newIterations > 2000000000) {
              throw new IllegalArgumentException("Iterations cannot be greater than 2000000000");
            }

            if (newIterations > 1000000) {
              boolean proceed =
                  ActionFeedback.showConfirmationDialogBox(
                      "Are you sure you want to set the iterations to "
                          + newIterations
                          + "?\nThis may take a long time.");
              if (!proceed) {
                return;
              }
            }

            this.countOfSteps = newIterations;
          } catch (Exception e) {
            ActionFeedback.showErrorDialogBox(e.getMessage());
            ExceptionHandler.getInstance().logException(e, "Error in setting iterations");
          }
          constructFractal();
        }
        case "FetchFractals" -> this.navBar.addFractal(FileHandling.getFractalNames());
        case "DeleteFractal" -> FileHandling.deleteFractal((String) newValue);
        case "ChaosGameComplete" ->
            notifyObservers("ChaosGameComplete", currenChaosGameManager.getCanvas());
        case "runningChaosGame" -> notifyObservers("runningChaosGame", null);
        default -> {
        }
      }
    } catch (Exception e) {
      ActionFeedback.showErrorDialogBox(e.getMessage());
      ExceptionHandler.getInstance()
          .logException(e, "Error in observer where changedValueName is " + changedValueName);
    }
  }
}
