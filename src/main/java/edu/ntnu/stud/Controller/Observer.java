package edu.ntnu.stud.Controller;

/**
 * Interface for Observer.
 */
public interface Observer {
  /**
   * Method to update the observer.
   *
   * @param changedValueName the name of the value that has changed
   * @param newValue         the new value
   */
  void update(String changedValueName, Object newValue);
}
